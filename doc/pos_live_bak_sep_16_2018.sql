-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 16, 2018 at 07:07 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Table structure for table `branch_product`
--

CREATE TABLE `branch_product` (
  `branch_store_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `branch_store`
--

CREATE TABLE `branch_store` (
  `branch_store_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(13) NOT NULL,
  `branch_manager_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `description`, `created`) VALUES
(1, 'Clothes', '', '2018-07-16 21:35:44'),
(2, 'Umbrella', '', '2018-07-19 00:00:18'),
(3, 'Underware', '', '2018-08-24 23:17:58'),
(4, 'Ornaments', '', '2018-09-02 18:48:27'),
(5, 'Footwear', '', '2018-09-03 21:40:36'),
(6, 'Bag', '', '2018-09-07 16:10:19'),
(7, 'Sunglass', '', '2018-09-07 16:10:40'),
(8, 'Money bag', '', '2018-09-07 16:11:10'),
(9, 'Watch', '', '2018-09-07 16:12:06'),
(10, 'Showpicec', '', '2018-09-07 16:12:40'),
(11, 'Belt', '', '2018-09-07 16:17:32'),
(12, 'Socks', '', '2018-09-07 16:18:16');

-- --------------------------------------------------------

--
-- Table structure for table `company_info`
--

CREATE TABLE `company_info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `country` varchar(80) NOT NULL,
  `zipcode` varchar(60) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_info`
--

INSERT INTO `company_info` (`id`, `name`, `address`, `city`, `country`, `zipcode`, `phone`) VALUES
(1, 'Nobabee Style', 'Khadiza palace&#44; Square Masterbari&#44;\r\nValuka', 'Mymensingh', 'Bangladesh', '2200', '01729019223');

-- --------------------------------------------------------

--
-- Table structure for table `cost`
--

CREATE TABLE `cost` (
  `cost_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `total_amount` float NOT NULL,
  `issue_date` datetime NOT NULL,
  `payment_status` varchar(150) NOT NULL,
  `due_amount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coustomer`
--

CREATE TABLE `coustomer` (
  `coustomer_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `email` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coustomer`
--

INSERT INTO `coustomer` (`coustomer_id`, `name`, `phone`, `email`) VALUES
(1, 'Customer', 'None', 'None');

-- --------------------------------------------------------

--
-- Table structure for table `discount`
--

CREATE TABLE `discount` (
  `discount_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `amount` float NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `discount_product`
--

CREATE TABLE `discount_product` (
  `product_id` int(11) NOT NULL,
  `discount_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `product_id` int(11) NOT NULL,
  `supply_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `order_id` int(11) NOT NULL,
  `discount` float NOT NULL,
  `coustomer_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `total_amount` varchar(45) NOT NULL,
  `payment_type` varchar(60) NOT NULL,
  `user_used_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`order_id`, `discount`, `coustomer_id`, `status`, `created`, `total_amount`, `payment_type`, `user_used_id`) VALUES
(1, 500, 1, 1, '2018-09-08 18:11:44', '1550', 'cash', 3),
(2, 50, 1, 1, '2018-09-08 19:00:08', '1200', 'cash', 3),
(3, 100, 1, 1, '2018-09-09 17:01:21', '500', 'cash', 3),
(4, 250, 1, 1, '2018-09-09 17:10:58', '1700', 'cash', 3),
(5, 90, 1, 1, '2018-09-09 17:44:30', '760', 'cash', 3),
(6, 10, 1, 1, '2018-09-09 18:00:57', '550', 'cash', 3),
(7, 180, 1, 1, '2018-09-09 19:10:59', '500', 'cash', 3),
(8, 50, 1, 1, '2018-09-09 21:03:11', '450', 'cash', 3),
(9, 50, 1, 1, '2018-09-09 21:48:09', '800', 'cash', 3),
(10, 10, 1, 1, '2018-09-09 21:54:04', '-10', 'cash', 3),
(11, 10, 1, 1, '2018-09-09 21:56:13', '-10', 'cash', 3),
(12, 100, 1, 1, '2018-09-10 15:17:43', '850', 'cash', 3),
(13, 100, 1, 1, '2018-09-10 15:19:24', '1150', 'cash', 3),
(14, 250, 1, 1, '2018-09-10 19:50:07', '2700', 'cash', 3),
(15, 50, 1, 1, '2018-09-10 21:09:18', '300', 'cash', 3),
(16, 390, 1, 1, '2018-09-11 10:54:29', '1000', 'cash', 3),
(17, 100, 1, 1, '2018-09-11 11:40:38', '1850', 'cash', 3),
(18, 110, 1, 1, '2018-09-11 11:53:39', '720', 'cash', 3),
(19, 50, 1, 1, '2018-09-11 16:31:47', '370', 'cash', 3),
(20, 130, 1, 1, '2018-09-11 18:39:50', '1150', 'cash', 3),
(21, 150, 1, 1, '2018-09-11 19:22:42', '800', 'cash', 3),
(22, 70, 1, 1, '2018-09-12 10:26:55', '300', 'cash', 3),
(23, 10, 1, 1, '2018-09-12 11:42:49', '100', 'cash', 3),
(24, 10, 1, 1, '2018-09-12 17:44:04', '100', 'cash', 3),
(25, 50, 1, 1, '2018-09-12 17:54:11', '1000', 'cash', 3),
(26, 100, 1, 1, '2018-09-12 18:34:52', '1050', 'cash', 3),
(27, 30, 1, 1, '2018-09-12 18:50:10', '230', 'cash', 3),
(28, 50, 1, 1, '2018-09-12 19:57:57', '200', 'cash', 3),
(29, 190, 1, 1, '2018-09-12 20:03:36', '1400', 'cash', 3),
(30, 30, 1, 1, '2018-09-12 20:44:31', '1450', 'cash', 3),
(31, 100, 1, 1, '2018-09-12 21:26:20', '1350', 'cash', 3),
(32, 0, 1, 1, '2018-09-13 18:30:12', '250', 'cash', 3),
(33, 80, 1, 1, '2018-09-13 19:27:16', '800', 'cash', 3),
(34, 90, 1, 1, '2018-09-13 19:35:24', '1150', 'cash', 3),
(35, 5, 1, 1, '2018-09-13 19:36:55', '115', 'cash', 3),
(36, 20, 1, 1, '2018-09-13 19:40:44', '540', 'cash', 3),
(37, 70, 1, 1, '2018-09-13 19:44:16', '520', 'cash', 3),
(38, 2, 1, 1, '2018-09-13 19:47:44', '558', 'cash', 3),
(39, 50, 1, 1, '2018-09-13 20:30:02', '800', 'cash', 3),
(40, 10, 1, 1, '2018-09-13 20:32:18', '100', 'cash', 3),
(41, 200, 1, 1, '2018-09-13 20:49:29', '1150', 'cash', 3),
(42, 10, 1, 1, '2018-09-14 18:14:45', '100', 'cash', 3),
(43, 10, 1, 1, '2018-09-14 18:33:59', '100', 'cash', 3),
(44, 90, 1, 1, '2018-09-14 18:37:03', '1500', 'cash', 3),
(45, 70, 1, 1, '2018-09-14 18:41:42', '1750', 'cash', 3),
(46, 50, 1, 1, '2018-09-14 18:45:54', '600', 'cash', 3),
(47, 10, 1, 1, '2018-09-14 18:49:12', '100', 'cash', 3),
(48, 50, 1, 1, '2018-09-14 19:38:40', '800', 'cash', 3),
(49, 50, 1, 1, '2018-09-14 19:53:07', '320', 'cash', 3),
(50, 120, 1, 1, '2018-09-14 20:04:36', '1050', 'cash', 3),
(51, 20, 1, 1, '2018-09-14 20:23:31', '540', 'cash', 3),
(52, 190, 1, 1, '2018-09-14 20:26:02', '1700', 'cash', 3),
(53, 30, 1, 1, '2018-09-14 20:33:56', '350', 'cash', 3),
(54, 140, 1, 1, '2018-09-15 11:52:56', '950', 'cash', 3),
(55, 10, 1, 1, '2018-09-15 14:54:50', '330', 'cash', 3),
(56, 10, 1, 1, '2018-09-15 16:20:20', '100', 'cash', 3),
(57, 180, 1, 1, '2018-09-15 18:47:14', '1970', 'cash', 3),
(58, 40, 1, 1, '2018-09-15 19:14:20', '520', 'cash', 3),
(59, 50, 1, 1, '2018-09-15 19:53:38', '300', 'cash', 3),
(60, 40, 1, 1, '2018-09-15 21:06:11', '850', 'cash', 3);

-- --------------------------------------------------------

--
-- Table structure for table `order_detail`
--

CREATE TABLE `order_detail` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `sub_total` float NOT NULL,
  `remark` text NOT NULL,
  `discount` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_detail`
--

INSERT INTO `order_detail` (`order_id`, `product_id`, `quantity`, `sub_total`, `remark`, `discount`) VALUES
(1, 244, 1, 1790, 'None', 0),
(1, 266, 1, 260, 'None', 0),
(2, 27, 1, 1250, 'None', 0),
(3, 90, 1, 600, 'None', 0),
(4, 290, 1, 1950, 'None', 0),
(5, 465, 1, 850, 'None', 0),
(6, 520, 1, 560, 'None', 0),
(7, 119, 1, 680, 'None', 0),
(8, 195, 1, 250, 'None', 0),
(8, 195, 1, 250, 'None', 0),
(9, 122, 1, 850, 'None', 0),
(10, 198, 0, 0, 'None', 0),
(11, 198, 0, 0, 'None', 0),
(12, 24, 1, 950, 'None', 0),
(13, 27, 1, 1250, 'None', 0),
(14, 58, 1, 390, 'None', 0),
(14, 5, 1, 1850, 'None', 0),
(14, 354, 1, 180, 'None', 0),
(14, 354, 1, 180, 'None', 0),
(14, 256, 1, 350, 'None', 0),
(15, 530, 1, 350, 'None', 0),
(16, 222, 1, 1390, 'None', 0),
(17, 28, 1, 1950, 'None', 0),
(18, 196, 1, 270, 'None', 0),
(18, 520, 1, 560, 'None', 0),
(19, 555, 1, 420, 'None', 0),
(20, 449, 1, 1280, 'None', 0),
(21, 24, 1, 950, 'None', 0),
(22, 531, 1, 370, 'None', 0),
(23, 198, 1, 110, 'None', 0),
(24, 198, 1, 110, 'None', 0),
(25, 410, 1, 1050, 'None', 0),
(26, 578, 1, 1150, 'None', 0),
(27, 272, 1, 260, 'None', 0),
(28, 303, 1, 250, 'None', 0),
(29, 446, 1, 1590, 'None', 0),
(30, 36, 1, 1480, 'None', 0),
(31, 283, 1, 1450, 'None', 0),
(32, 300, 1, 250, 'None', 0),
(33, 175, 1, 880, 'None', 0),
(34, 214, 1, 1240, 'None', 0),
(35, 197, 1, 120, 'None', 0),
(36, 520, 1, 560, 'None', 0),
(37, 147, 1, 590, 'None', 0),
(38, 520, 1, 560, 'None', 0),
(39, 483, 1, 850, 'None', 0),
(40, 556, 1, 110, 'None', 0),
(41, 292, 1, 1350, 'None', 0),
(42, 198, 1, 110, 'None', 0),
(43, 198, 1, 110, 'None', 0),
(44, 446, 1, 1590, 'None', 0),
(45, 1, 1, 1820, 'None', 0),
(46, 275, 1, 650, 'None', 0),
(47, 198, 1, 110, 'None', 0),
(48, 501, 1, 850, 'None', 0),
(49, 529, 1, 370, 'None', 0),
(50, 394, 1, 720, 'None', 0),
(50, 241, 1, 450, 'None', 0),
(51, 520, 1, 560, 'None', 0),
(52, 286, 1, 1890, 'None', 0),
(53, 586, 1, 380, 'None', 0),
(54, 386, 1, 1090, 'None', 0),
(55, 539, 1, 340, 'None', 0),
(56, 198, 1, 110, 'None', 0),
(57, 26, 1, 2150, 'None', 0),
(58, 520, 1, 560, 'None', 0),
(59, 536, 1, 350, 'None', 0),
(60, 94, 1, 890, 'None', 0);

-- --------------------------------------------------------

--
-- Table structure for table `permission`
--

CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL,
  `permission` varchar(60) NOT NULL,
  `permission_description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `unit_price` float NOT NULL,
  `sale_price` float NOT NULL,
  `active` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `edited` datetime NOT NULL,
  `category_id` int(11) NOT NULL,
  `image_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `title`, `description`, `unit_price`, `sale_price`, `active`, `created`, `edited`, `category_id`, `image_url`) VALUES
(1, 'AB-01', '', 1150, 1820, 1, '2018-09-07 16:19:08', '2018-09-10 11:11:34', 6, 'images/default_product_image.jpg'),
(2, 'AB-02', '', 750, 1150, 1, '2018-09-07 16:19:50', '2018-09-10 11:11:41', 6, 'images/default_product_image.jpg'),
(3, 'AB-1', '', 1300, 1980, 1, '2018-09-07 16:21:26', '2018-09-10 11:10:51', 6, 'images/default_product_image.jpg'),
(4, 'AB-160-46', '', 750, 1150, 1, '2018-09-07 16:22:53', '2018-09-07 16:22:53', 5, 'images/default_product_image.jpg'),
(5, 'AB-2', '', 1200, 1850, 1, '2018-09-07 16:23:22', '2018-09-10 11:11:06', 6, 'images/default_product_image.jpg'),
(6, 'AB-30829-13', '', 850, 1360, 1, '2018-09-07 16:24:10', '2018-09-07 16:24:10', 5, 'images/default_product_image.jpg'),
(7, 'AB-3526-3', '', 800, 1350, 1, '2018-09-07 16:24:39', '2018-09-07 16:24:39', 5, 'images/default_product_image.jpg'),
(8, 'AF-1', '', 500, 880, 1, '2018-09-07 16:25:54', '2018-09-10 11:09:43', 1, 'images/default_product_image.jpg'),
(9, 'BEC-10', '', 750, 1150, 0, '2018-09-07 16:26:40', '2018-09-07 16:26:40', 6, 'images/default_product_image.jpg'),
(10, 'AT-05', '', 800, 1350, 0, '2018-09-07 16:27:15', '2018-09-07 16:27:15', 5, 'images/default_product_image.jpg'),
(11, 'AK-01', '', 30, 50, 1, '2018-09-07 16:28:43', '2018-09-07 16:28:43', 12, 'images/default_product_image.jpg'),
(12, 'AK-02', '', 60, 150, 1, '2018-09-07 16:29:15', '2018-09-07 16:29:15', 12, 'images/default_product_image.jpg'),
(13, 'AK-03', '', 60, 130, 1, '2018-09-07 16:29:40', '2018-09-07 16:29:40', 12, 'images/default_product_image.jpg'),
(14, 'AT-04', '', 500, 950, 1, '2018-09-07 16:30:47', '2018-09-07 16:33:36', 5, 'images/default_product_image.jpg'),
(15, 'AT-05', '', 800, 1350, 1, '2018-09-07 16:32:25', '2018-09-07 16:33:45', 5, 'images/default_product_image.jpg'),
(16, 'BC-01', '', 800, 1290, 1, '2018-09-07 16:38:09', '2018-09-07 16:38:09', 6, 'images/default_product_image.jpg'),
(17, 'BEC-11', '', 650, 1090, 1, '2018-09-07 16:39:09', '2018-09-07 16:39:09', 6, 'images/default_product_image.jpg'),
(18, 'BEC-3', '', 950, 1750, 1, '2018-09-07 16:40:00', '2018-09-11 11:12:05', 6, 'images/default_product_image.jpg'),
(19, 'BEC-4', '', 1300, 2150, 1, '2018-09-07 16:43:43', '2018-09-07 16:43:43', 6, 'images/default_product_image.jpg'),
(21, 'BEC-7', '', 750, 1250, 1, '2018-09-07 16:47:44', '2018-09-07 16:47:44', 6, 'images/default_product_image.jpg'),
(22, 'BEC-8', '', 900, 1450, 1, '2018-09-07 16:48:49', '2018-09-07 16:48:49', 6, 'images/default_product_image.jpg'),
(23, 'BELT-1', '', 270, 500, 1, '2018-09-07 16:50:55', '2018-09-07 16:50:55', 11, 'images/default_product_image.jpg'),
(24, 'BEST-1', '', 500, 950, 1, '2018-09-07 16:52:04', '2018-09-09 15:32:33', 6, 'images/default_product_image.jpg'),
(25, 'BEST-10', '', 550, 880, 1, '2018-09-07 16:52:58', '2018-09-07 16:52:58', 6, 'images/default_product_image.jpg'),
(26, 'BEST-2', '', 1300, 2150, 1, '2018-09-07 16:54:01', '2018-09-07 16:54:01', 6, 'images/default_product_image.jpg'),
(27, 'BEST-3', '', 750, 1250, 1, '2018-09-07 16:57:19', '2018-09-07 16:57:19', 6, 'images/default_product_image.jpg'),
(28, 'BEST-4', '', 1150, 1950, 1, '2018-09-07 16:58:13', '2018-09-07 16:58:13', 6, 'images/default_product_image.jpg'),
(29, 'BEST-5', '', 700, 1450, 1, '2018-09-07 16:59:01', '2018-09-07 16:59:01', 6, 'images/default_product_image.jpg'),
(30, 'BEST-6', '', 730, 1350, 1, '2018-09-07 16:59:59', '2018-09-07 16:59:59', 6, 'images/default_product_image.jpg'),
(31, 'DE-06', '', 400, 680, 1, '2018-09-07 17:12:42', '2018-09-07 17:12:42', 5, 'images/default_product_image.jpg'),
(32, 'DE-5886', '', 750, 1200, 1, '2018-09-07 17:13:46', '2018-09-07 17:13:46', 5, 'images/default_product_image.jpg'),
(33, 'DE-8160', '', 750, 1200, 1, '2018-09-07 17:14:33', '2018-09-07 17:14:33', 5, 'images/default_product_image.jpg'),
(34, 'DPLW-01', '', 1050, 1850, 1, '2018-09-07 17:15:15', '2018-09-07 17:15:43', 9, 'images/default_product_image.jpg'),
(35, 'DS-03', '', 700, 1150, 1, '2018-09-07 17:16:34', '2018-09-07 17:16:34', 5, 'images/default_product_image.jpg'),
(36, 'DS-26', '', 900, 1480, 1, '2018-09-07 17:17:35', '2018-09-07 17:17:35', 5, 'images/default_product_image.jpg'),
(37, 'DS-33', '', 900, 1480, 1, '2018-09-07 17:18:22', '2018-09-12 12:47:54', 5, 'images/default_product_image.jpg'),
(38, 'DS-52A', '', 1100, 2150, 1, '2018-09-07 17:21:20', '2018-09-07 17:21:20', 5, 'images/default_product_image.jpg'),
(39, 'DS-52B', '', 1100, 2050, 1, '2018-09-07 17:21:54', '2018-09-07 17:22:48', 5, 'images/default_product_image.jpg'),
(40, 'DS-52C', '', 1100, 2150, 1, '2018-09-07 17:22:25', '2018-09-07 17:22:25', 5, 'images/default_product_image.jpg'),
(41, 'DS-54A', '', 1225, 2190, 1, '2018-09-07 17:24:11', '2018-09-07 17:24:11', 5, 'images/default_product_image.jpg'),
(42, 'DS-54C', '', 1225, 2190, 1, '2018-09-07 17:25:17', '2018-09-07 17:25:17', 5, 'images/default_product_image.jpg'),
(43, 'DS-54D', '', 1225, 2250, 1, '2018-09-07 17:25:47', '2018-09-07 17:25:47', 5, 'images/default_product_image.jpg'),
(44, 'DS-64', '', 900, 1600, 1, '2018-09-07 17:26:09', '2018-09-07 17:26:09', 5, 'images/default_product_image.jpg'),
(45, 'DS-68', '', 1100, 2150, 1, '2018-09-07 17:26:48', '2018-09-07 17:26:48', 5, 'images/default_product_image.jpg'),
(46, 'DS-68A', '', 1000, 1790, 1, '2018-09-07 17:28:12', '2018-09-07 17:28:12', 5, 'images/default_product_image.jpg'),
(47, 'DS-68B', '', 1000, 1700, 1, '2018-09-07 17:29:06', '2018-09-07 17:29:06', 5, 'images/default_product_image.jpg'),
(48, 'DS-Action', '', 350, 570, 1, '2018-09-07 17:29:50', '2018-09-07 17:29:50', 5, 'images/default_product_image.jpg'),
(49, 'DS-Paruma', '', 200, 420, 1, '2018-09-07 17:30:37', '2018-09-07 17:30:37', 5, 'images/default_product_image.jpg'),
(50, 'DS-Paruma1', '', 190, 370, 1, '2018-09-07 17:31:11', '2018-09-07 17:31:11', 5, 'images/default_product_image.jpg'),
(51, 'DS-Silpi', '', 180, 340, 1, '2018-09-07 17:31:38', '2018-09-07 17:31:38', 5, 'images/default_product_image.jpg'),
(52, 'DS-Tokyo', '', 180, 340, 1, '2018-09-07 17:32:37', '2018-09-07 17:32:37', 5, 'images/default_product_image.jpg'),
(53, 'DWC-22', '', 180, 350, 1, '2018-09-07 17:33:22', '2018-09-07 17:34:15', 9, 'images/default_product_image.jpg'),
(54, 'DWC-28', '', 180, 350, 1, '2018-09-07 17:33:58', '2018-09-09 15:32:16', 9, 'images/default_product_image.jpg'),
(55, 'DWC-41', '', 180, 290, 1, '2018-09-07 17:35:29', '2018-09-07 17:35:29', 9, 'images/default_product_image.jpg'),
(56, 'EAR TOP-12', '', 270, 480, 1, '2018-09-07 17:36:13', '2018-09-07 17:36:13', 4, 'images/default_product_image.jpg'),
(57, 'EAR TOP-14', '', 260, 470, 1, '2018-09-07 17:37:12', '2018-09-07 17:37:12', 4, 'images/default_product_image.jpg'),
(58, 'EAR TOP-2', '', 195, 390, 1, '2018-09-07 17:37:46', '2018-09-07 17:37:46', 4, 'images/default_product_image.jpg'),
(59, 'EAR TOP-20', '', 200, 380, 1, '2018-09-07 17:38:49', '2018-09-07 17:38:49', 4, 'images/default_product_image.jpg'),
(60, 'EAR TOP-21', '', 195, 370, 1, '2018-09-07 17:39:21', '2018-09-07 17:39:21', 4, 'images/default_product_image.jpg'),
(61, 'EAR TOP-24', '', 320, 520, 1, '2018-09-07 17:40:16', '2018-09-07 17:40:16', 4, 'images/default_product_image.jpg'),
(62, 'EAR TOP-26', '', 210, 350, 1, '2018-09-07 17:40:50', '2018-09-07 17:40:50', 4, 'images/default_product_image.jpg'),
(63, 'EAR TOP-28', '', 315, 500, 1, '2018-09-07 18:08:43', '2018-09-07 18:08:43', 4, 'images/default_product_image.jpg'),
(64, 'EAR TOP-3', '', 280, 390, 1, '2018-09-07 18:11:22', '2018-09-07 18:11:22', 4, 'images/default_product_image.jpg'),
(65, 'EAR TOP-32', '', 150, 250, 1, '2018-09-07 18:12:02', '2018-09-07 18:12:02', 4, 'images/default_product_image.jpg'),
(66, 'EAR TOP-33', '', 290, 500, 1, '2018-09-07 18:12:39', '2018-09-07 18:12:39', 4, 'images/default_product_image.jpg'),
(67, 'EAR TOP-5', '', 180, 320, 1, '2018-09-07 18:13:18', '2018-09-07 18:13:18', 4, 'images/default_product_image.jpg'),
(68, 'EAR TOP-6', '', 200, 390, 1, '2018-09-07 18:15:12', '2018-09-07 18:15:12', 4, 'images/default_product_image.jpg'),
(69, 'ES-13265', '', 2450, 3250, 1, '2018-09-07 18:40:26', '2018-09-07 18:40:26', 5, 'images/default_product_image.jpg'),
(70, 'ES-16038', '', 1200, 1950, 1, '2018-09-07 18:40:51', '2018-09-07 20:06:05', 5, 'images/default_product_image.jpg'),
(71, 'ES-1636', '', 1150, 1780, 1, '2018-09-07 18:41:16', '2018-09-07 20:06:15', 5, 'images/default_product_image.jpg'),
(72, 'ES-1637', '', 1150, 1780, 1, '2018-09-07 18:41:50', '2018-09-07 18:41:50', 5, 'images/default_product_image.jpg'),
(73, 'ES-2021-1', '', 850, 1390, 1, '2018-09-07 18:54:45', '2018-09-07 18:54:45', 5, 'images/default_product_image.jpg'),
(74, 'ES-881', '', 1100, 1800, 1, '2018-09-07 19:01:20', '2018-09-07 19:01:20', 5, 'images/default_product_image.jpg'),
(75, 'ES-98090', '', 1800, 2700, 1, '2018-09-07 19:02:19', '2018-09-07 19:02:19', 5, 'images/default_product_image.jpg'),
(76, 'ES-9603', '', 1750, 2790, 1, '2018-09-07 19:02:52', '2018-09-07 19:02:52', 5, 'images/default_product_image.jpg'),
(77, 'ES-CLM-157', '', 1400, 2190, 1, '2018-09-07 19:04:29', '2018-09-07 19:04:29', 5, 'images/default_product_image.jpg'),
(78, 'ES-1319', '', 700, 1120, 1, '2018-09-07 19:04:55', '2018-09-07 19:04:55', 5, 'images/default_product_image.jpg'),
(79, 'ES-1425', '', 1000, 1590, 1, '2018-09-07 19:05:44', '2018-09-07 19:05:44', 5, 'images/default_product_image.jpg'),
(80, 'ES-483', '', 1000, 1590, 1, '2018-09-07 19:06:11', '2018-09-07 19:06:11', 5, 'images/default_product_image.jpg'),
(81, 'ES-JX008-1', '', 900, 1950, 1, '2018-09-07 19:06:54', '2018-09-14 14:28:03', 5, 'images/default_product_image.jpg'),
(82, 'ES-K117', '', 700, 1350, 1, '2018-09-07 19:07:49', '2018-09-07 19:07:49', 5, 'images/default_product_image.jpg'),
(83, 'ES-NV117', '', 900, 1450, 1, '2018-09-07 19:08:22', '2018-09-07 19:08:22', 5, 'images/default_product_image.jpg'),
(84, 'ES-PONS2', '', 350, 650, 1, '2018-09-07 19:09:21', '2018-09-07 19:09:21', 5, 'images/default_product_image.jpg'),
(85, 'ES-122', '', 1000, 1590, 1, '2018-09-07 19:09:55', '2018-09-07 19:09:55', 5, 'images/default_product_image.jpg'),
(86, 'ES-SF1702', '', 1650, 2790, 1, '2018-09-07 19:10:37', '2018-09-07 19:10:37', 5, 'images/default_product_image.jpg'),
(87, 'ES-SF1701', '', 1650, 2790, 1, '2018-09-07 19:11:14', '2018-09-07 19:11:14', 5, 'images/default_product_image.jpg'),
(88, 'ES-T1273', '', 2400, 3300, 1, '2018-09-07 19:11:52', '2018-09-07 19:11:52', 5, 'images/default_product_image.jpg'),
(89, 'ES-TX023', '', 1000, 1550, 1, '2018-09-07 19:12:25', '2018-09-07 19:12:25', 5, 'images/default_product_image.jpg'),
(90, 'Gents pass', '', 375, 600, 1, '2018-09-07 19:15:44', '2018-09-07 19:15:44', 8, 'images/default_product_image.jpg'),
(91, 'IF-01', '', 500, 850, 1, '2018-09-07 19:22:52', '2018-09-07 19:22:52', 1, 'images/default_product_image.jpg'),
(92, 'IF-02', '', 270, 590, 1, '2018-09-07 19:23:33', '2018-09-07 19:23:33', 1, 'images/default_product_image.jpg'),
(93, 'IF-03', '', 700, 1350, 1, '2018-09-07 19:24:42', '2018-09-07 19:24:42', 1, 'images/default_product_image.jpg'),
(94, 'IF-04', '', 500, 890, 1, '2018-09-07 19:25:11', '2018-09-07 19:25:11', 1, 'images/default_product_image.jpg'),
(95, 'IF-05', '', 800, 1450, 1, '2018-09-07 19:25:40', '2018-09-07 19:25:40', 1, 'images/default_product_image.jpg'),
(96, 'IF-06', '', 400, 790, 1, '2018-09-07 19:26:10', '2018-09-07 19:26:10', 1, 'images/default_product_image.jpg'),
(97, 'IF1-1', '', 1100, 2150, 1, '2018-09-07 19:34:22', '2018-09-07 19:34:22', 1, 'images/default_product_image.jpg'),
(98, 'IF1-2', '', 1900, 3200, 1, '2018-09-07 19:34:53', '2018-09-07 19:34:53', 1, 'images/default_product_image.jpg'),
(99, 'IF1-3', '', 1500, 2350, 1, '2018-09-07 19:35:27', '2018-09-07 19:35:27', 1, 'images/default_product_image.jpg'),
(100, 'IF1-4', '', 2400, 3700, 1, '2018-09-07 19:36:18', '2018-09-07 19:36:18', 1, 'images/default_product_image.jpg'),
(101, 'IF1-5', '', 1800, 3050, 1, '2018-09-07 19:36:54', '2018-09-07 19:36:54', 1, 'images/default_product_image.jpg'),
(102, 'IF2-1', '', 6, 1100, 1, '2018-09-07 19:37:36', '2018-09-07 19:37:36', 1, 'images/default_product_image.jpg'),
(103, 'IF2-2', '', 800, 1450, 1, '2018-09-07 19:38:16', '2018-09-07 19:38:16', 1, 'images/default_product_image.jpg'),
(104, 'IF2-5', '', 1700, 2750, 1, '2018-09-07 19:38:53', '2018-09-07 19:38:53', 1, 'images/default_product_image.jpg'),
(105, 'IF2-6', '', 1500, 2690, 1, '2018-09-07 19:39:19', '2018-09-07 19:39:19', 1, 'images/default_product_image.jpg'),
(106, 'IF2-7', '', 950, 1550, 1, '2018-09-07 19:39:42', '2018-09-07 19:39:42', 1, 'images/default_product_image.jpg'),
(107, 'IF2-9', '', 600, 1150, 1, '2018-09-07 19:40:39', '2018-09-07 19:40:39', 1, 'images/default_product_image.jpg'),
(108, 'IG-24', '', 500, 950, 1, '2018-09-07 19:41:06', '2018-09-07 19:41:06', 1, 'images/default_product_image.jpg'),
(109, 'JP-17', '', 450, 750, 1, '2018-09-08 10:36:03', '2018-09-08 10:36:03', 8, 'images/default_product_image.jpg'),
(110, 'JP-34', '', 400, 650, 1, '2018-09-08 10:36:25', '2018-09-08 10:36:25', 8, 'images/default_product_image.jpg'),
(111, 'JP-35', '', 500, 850, 1, '2018-09-08 10:36:47', '2018-09-08 10:36:47', 8, 'images/default_product_image.jpg'),
(112, 'JP-39', '', 500, 820, 1, '2018-09-08 10:37:08', '2018-09-08 10:37:08', 8, 'images/default_product_image.jpg'),
(113, 'JP-BELT J', '', 230, 370, 1, '2018-09-08 10:38:08', '2018-09-08 10:38:08', 11, 'images/default_product_image.jpg'),
(114, 'JP-BELT K', '', 180, 300, 1, '2018-09-08 10:38:37', '2018-09-08 10:38:37', 11, 'images/default_product_image.jpg'),
(115, 'JP-BELT L', '', 230, 370, 1, '2018-09-08 10:39:09', '2018-09-08 10:39:09', 11, 'images/default_product_image.jpg'),
(116, 'JP-BELT N', '', 150, 280, 1, '2018-09-08 10:39:40', '2018-09-08 10:39:40', 11, 'images/default_product_image.jpg'),
(117, 'JP-MB A', '', 450, 750, 1, '2018-09-08 10:40:40', '2018-09-08 10:40:40', 8, 'images/default_product_image.jpg'),
(118, 'JP-MB 1', '', 230, 420, 1, '2018-09-08 10:41:49', '2018-09-08 10:41:49', 8, 'images/default_product_image.jpg'),
(119, 'JP-MB C', '', 420, 680, 1, '2018-09-08 10:42:38', '2018-09-08 10:42:38', 8, 'images/default_product_image.jpg'),
(120, 'JP-MB H', '', 350, 560, 1, '2018-09-08 10:43:07', '2018-09-08 10:43:07', 8, 'images/default_product_image.jpg'),
(121, 'JT-06-8', '', 1100, 1780, 1, '2018-09-08 12:08:28', '2018-09-08 12:08:28', 5, 'images/default_product_image.jpg'),
(122, 'JT-121', '', 500, 850, 1, '2018-09-08 12:09:05', '2018-09-09 15:31:44', 3, 'images/default_product_image.jpg'),
(123, 'JT-125', '', 950, 1550, 1, '2018-09-08 12:09:33', '2018-09-08 12:09:33', 5, 'images/default_product_image.jpg'),
(124, 'JT-1539', '', 600, 980, 1, '2018-09-08 12:10:33', '2018-09-08 12:10:33', 5, 'images/default_product_image.jpg'),
(125, 'JT-1704', '', 320, 550, 1, '2018-09-08 12:11:05', '2018-09-08 12:11:05', 5, 'images/default_product_image.jpg'),
(126, 'JT-17570', '', 800, 1350, 1, '2018-09-08 12:11:45', '2018-09-08 12:11:45', 5, 'images/default_product_image.jpg'),
(127, 'JT-3211', '', 325, 590, 1, '2018-09-08 12:12:25', '2018-09-08 12:12:25', 5, 'images/default_product_image.jpg'),
(128, 'JT-6001', '', 520, 880, 1, '2018-09-08 12:12:53', '2018-09-08 12:12:53', 5, 'images/default_product_image.jpg'),
(129, 'JT-602-4', '', 500, 850, 1, '2018-09-08 12:13:28', '2018-09-08 12:13:28', 5, 'images/default_product_image.jpg'),
(130, 'JT-606-09', '', 1000, 1620, 1, '2018-09-08 12:16:01', '2018-09-08 12:16:01', 5, 'images/default_product_image.jpg'),
(131, 'JT-614', '', 300, 450, 1, '2018-09-08 12:16:26', '2018-09-08 12:16:26', 5, 'images/default_product_image.jpg'),
(132, 'JT-656', '', 1300, 2150, 1, '2018-09-08 12:17:09', '2018-09-08 12:17:09', 5, 'images/default_product_image.jpg'),
(133, 'JT-727', '', 700, 1150, 1, '2018-09-08 12:17:34', '2018-09-08 12:17:34', 5, 'images/default_product_image.jpg'),
(134, 'JT-761', '', 700, 1150, 1, '2018-09-08 12:17:56', '2018-09-08 12:17:56', 5, 'images/default_product_image.jpg'),
(135, 'JT-7708', '', 650, 1090, 1, '2018-09-08 12:18:28', '2018-09-08 12:18:28', 5, 'images/default_product_image.jpg'),
(136, 'JT-8-0050', '', 1600, 2560, 1, '2018-09-08 12:20:28', '2018-09-08 12:20:28', 5, 'images/default_product_image.jpg'),
(137, 'JT-801', '', 470, 790, 1, '2018-09-08 12:20:55', '2018-09-08 12:20:55', 5, 'images/default_product_image.jpg'),
(138, 'JT-837-1', '', 800, 1350, 1, '2018-09-08 12:21:24', '2018-09-08 12:21:24', 5, 'images/default_product_image.jpg'),
(139, 'JT-837-18', '', 800, 1350, 1, '2018-09-08 12:22:07', '2018-09-08 12:22:07', 5, 'images/default_product_image.jpg'),
(140, 'JT-8811', '', 480, 790, 1, '2018-09-08 12:22:37', '2018-09-08 12:22:37', 5, 'images/default_product_image.jpg'),
(141, 'JT-885', '', 380, 620, 1, '2018-09-08 12:23:08', '2018-09-08 12:23:08', 5, 'images/default_product_image.jpg'),
(142, 'JT-888', '', 325, 590, 1, '2018-09-08 12:23:42', '2018-09-08 12:23:42', 5, 'images/default_product_image.jpg'),
(143, 'JT-90029', '', 900, 1590, 1, '2018-09-08 12:34:14', '2018-09-08 12:34:14', 5, 'images/default_product_image.jpg'),
(144, 'JT-90033', '', 900, 1590, 1, '2018-09-08 12:34:43', '2018-09-08 12:34:43', 5, 'images/default_product_image.jpg'),
(145, 'JT-901', '', 320, 520, 1, '2018-09-08 12:35:08', '2018-09-08 12:35:08', 5, 'images/default_product_image.jpg'),
(146, 'JT-98269', '', 2650, 3980, 1, '2018-09-08 12:35:44', '2018-09-08 12:35:44', 5, 'images/default_product_image.jpg'),
(147, 'JT-9903', '', 320, 590, 1, '2018-09-08 12:36:10', '2018-09-08 12:36:10', 5, 'images/default_product_image.jpg'),
(148, 'JT-A07', '', 500, 850, 1, '2018-09-08 12:36:41', '2018-09-09 15:31:32', 3, 'images/default_product_image.jpg'),
(149, 'JT-A801', '', 370, 650, 1, '2018-09-08 12:37:20', '2018-09-08 12:37:20', 5, 'images/default_product_image.jpg'),
(150, 'JT-B-8818', '', 450, 750, 1, '2018-09-08 12:58:46', '2018-09-08 12:58:46', 5, 'images/default_product_image.jpg'),
(151, 'JT-FD-8832', '', 1600, 2560, 1, '2018-09-08 13:00:08', '2018-09-08 13:00:08', 5, 'images/default_product_image.jpg'),
(152, 'JT-HS0502-2', '', 1050, 1680, 1, '2018-09-08 13:01:52', '2018-09-08 13:01:52', 5, 'images/default_product_image.jpg'),
(153, 'JT-NIKE  PLASTIC', '', 225, 390, 1, '2018-09-08 13:03:24', '2018-09-08 13:03:24', 5, 'images/default_product_image.jpg'),
(154, 'JT-PL-556', '', 310, 560, 1, '2018-09-08 13:04:26', '2018-09-08 13:04:26', 5, 'images/default_product_image.jpg'),
(155, 'JT-RC5621', '', 2550, 3850, 1, '2018-09-08 13:05:45', '2018-09-08 13:05:45', 5, 'images/default_product_image.jpg'),
(156, 'JT-RS22', '', 2550, 3890, 1, '2018-09-08 13:06:56', '2018-09-08 13:06:56', 5, 'images/default_product_image.jpg'),
(158, 'JT-RY8629', '', 2350, 3650, 1, '2018-09-08 13:31:16', '2018-09-08 13:31:16', 5, 'images/default_product_image.jpg'),
(159, 'JT-Y732', '', 550, 950, 1, '2018-09-08 13:32:03', '2018-09-08 13:32:03', 5, 'images/default_product_image.jpg'),
(160, 'LAB-17129', '', 1100, 1680, 1, '2018-09-08 13:33:40', '2018-09-08 13:33:40', 5, 'images/default_product_image.jpg'),
(161, 'LAB-1756', '', 1100, 1680, 1, '2018-09-08 13:35:33', '2018-09-08 13:35:33', 5, 'images/default_product_image.jpg'),
(162, 'LAB-8019', '', 325, 590, 1, '2018-09-08 14:36:15', '2018-09-08 14:36:15', 5, 'images/default_product_image.jpg'),
(163, 'LAB-807', '', 470, 790, 1, '2018-09-08 14:36:55', '2018-09-08 14:36:55', 5, 'images/default_product_image.jpg'),
(164, 'LAB-8136', '', 325, 590, 1, '2018-09-08 14:49:12', '2018-09-08 14:49:12', 5, 'images/default_product_image.jpg'),
(165, 'LB-0389-585', '', 750, 1090, 1, '2018-09-08 14:49:53', '2018-09-08 14:49:53', 5, 'images/default_product_image.jpg'),
(166, 'LB-1000-46', '', 1200, 1950, 1, '2018-09-08 14:50:21', '2018-09-08 14:50:21', 5, 'images/default_product_image.jpg'),
(167, 'LB-102', '', 450, 720, 1, '2018-09-08 14:50:47', '2018-09-08 14:50:47', 5, 'images/default_product_image.jpg'),
(168, 'LB-11986235', '', 1000, 1600, 1, '2018-09-08 14:51:17', '2018-09-08 14:51:17', 5, 'images/default_product_image.jpg'),
(169, 'LB-240-15', '', 520, 880, 1, '2018-09-08 14:51:45', '2018-09-08 14:51:45', 5, 'images/default_product_image.jpg'),
(170, 'LB-32920235', '', 1000, 1600, 1, '2018-09-08 14:52:17', '2018-09-08 14:52:17', 5, 'images/default_product_image.jpg'),
(171, 'LB-6338', '', 500, 850, 1, '2018-09-08 14:52:41', '2018-09-08 14:52:41', 5, 'images/default_product_image.jpg'),
(172, 'LB-803-15', '', 750, 1160, 1, '2018-09-08 14:54:05', '2018-09-08 14:54:05', 5, 'images/default_product_image.jpg'),
(173, 'LB-A02', '', 400, 680, 1, '2018-09-08 14:54:27', '2018-09-08 14:54:27', 5, 'images/default_product_image.jpg'),
(174, 'LB-H241817', '', 500, 890, 1, '2018-09-08 14:54:55', '2018-09-08 14:54:55', 5, 'images/default_product_image.jpg'),
(175, 'LB-XZ-2', '', 520, 880, 1, '2018-09-08 14:55:49', '2018-09-08 14:55:49', 5, 'images/default_product_image.jpg'),
(176, 'LBB-121-3', '', 800, 1250, 1, '2018-09-08 14:56:31', '2018-09-08 14:56:31', 5, 'images/default_product_image.jpg'),
(177, 'LBB-1605-10', '', 550, 950, 1, '2018-09-08 14:56:54', '2018-09-08 14:56:54', 5, 'images/default_product_image.jpg'),
(178, 'LBB-188-8', '', 1000, 1680, 1, '2018-09-08 14:57:24', '2018-09-08 14:57:24', 5, 'images/default_product_image.jpg'),
(179, 'LBB-2121', '', 700, 1150, 1, '2018-09-08 14:58:20', '2018-09-08 14:58:20', 5, 'images/default_product_image.jpg'),
(180, 'LBB-33171', '', 850, 1350, 1, '2018-09-08 14:58:48', '2018-09-08 14:58:48', 5, 'images/default_product_image.jpg'),
(181, 'LBB-968-1', '', 550, 950, 1, '2018-09-08 14:59:17', '2018-09-08 14:59:17', 5, 'images/default_product_image.jpg'),
(182, 'LBB-8103', '', 450, 720, 1, '2018-09-08 14:59:41', '2018-09-08 14:59:41', 5, 'images/default_product_image.jpg'),
(183, 'LBB-8113', '', 450, 720, 1, '2018-09-08 15:00:03', '2018-09-08 15:00:03', 5, 'images/default_product_image.jpg'),
(184, 'LBB-8203', '', 450, 750, 1, '2018-09-08 15:00:27', '2018-09-08 15:00:27', 5, 'images/default_product_image.jpg'),
(185, 'LBB-83555', '', 800, 1250, 1, '2018-09-08 15:00:57', '2018-09-08 15:00:57', 5, 'images/default_product_image.jpg'),
(186, 'LBB-869-9', '', 550, 950, 1, '2018-09-08 15:01:22', '2018-09-08 15:01:22', 5, 'images/default_product_image.jpg'),
(187, 'LBB-968-8', '', 550, 950, 1, '2018-09-08 15:02:16', '2018-09-08 15:02:16', 5, 'images/default_product_image.jpg'),
(188, 'LBB-A-8', '', 450, 720, 1, '2018-09-08 15:02:45', '2018-09-08 15:02:45', 5, 'images/default_product_image.jpg'),
(189, 'LBB-F0086-21', '', 750, 1190, 1, '2018-09-08 15:03:20', '2018-09-08 15:03:20', 5, 'images/default_product_image.jpg'),
(190, 'LBB-F950', '', 1050, 1680, 1, '2018-09-08 15:03:53', '2018-09-08 15:03:53', 5, 'images/default_product_image.jpg'),
(191, 'LBB-W595-1', '', 750, 1150, 1, '2018-09-08 15:04:24', '2018-09-08 15:04:24', 5, 'images/default_product_image.jpg'),
(192, 'LBB-W595-5', '', 750, 1150, 1, '2018-09-08 15:04:50', '2018-09-08 15:04:50', 5, 'images/default_product_image.jpg'),
(193, 'MC-15', '', 170, 290, 1, '2018-09-08 15:06:20', '2018-09-08 15:06:20', 3, 'images/default_product_image.jpg'),
(194, 'MC-18', '', 90, 180, 1, '2018-09-08 15:06:47', '2018-09-08 15:06:47', 3, 'images/default_product_image.jpg'),
(195, 'MC-25', '', 120, 250, 1, '2018-09-08 15:07:13', '2018-09-08 15:07:13', 3, 'images/default_product_image.jpg'),
(196, 'MC-26', '', 150, 270, 1, '2018-09-08 15:07:52', '2018-09-09 15:31:00', 3, 'images/default_product_image.jpg'),
(197, 'MC-27', '', 55, 120, 1, '2018-09-08 15:09:06', '2018-09-08 15:09:06', 12, 'images/default_product_image.jpg'),
(198, 'MC-31', '', 55, 110, 1, '2018-09-08 15:11:25', '2018-09-08 15:11:25', 1, 'images/default_product_image.jpg'),
(199, 'MC-35', '', 100, 220, 1, '2018-09-08 15:11:52', '2018-09-08 15:11:52', 3, 'images/default_product_image.jpg'),
(200, 'MOD-1603', '', 500, 950, 1, '2018-09-08 15:13:03', '2018-09-08 15:13:03', 5, 'images/default_product_image.jpg'),
(201, 'MOD-170301-5', '', 1000, 1590, 1, '2018-09-08 15:13:38', '2018-09-08 15:13:38', 5, 'images/default_product_image.jpg'),
(202, 'MOD-2475-2', '', 800, 1240, 1, '2018-09-08 15:14:27', '2018-09-08 15:14:27', 5, 'images/default_product_image.jpg'),
(203, 'MOD-602-7', '', 500, 850, 1, '2018-09-08 15:15:11', '2018-09-08 15:15:11', 5, 'images/default_product_image.jpg'),
(204, 'MOD-603', '', 325, 590, 1, '2018-09-08 15:15:35', '2018-09-08 15:15:35', 5, 'images/default_product_image.jpg'),
(205, 'MOD-605', '', 700, 1090, 1, '2018-09-08 15:16:03', '2018-09-08 15:16:03', 5, 'images/default_product_image.jpg'),
(206, 'MOD-6229', '', 750, 1020, 1, '2018-09-08 15:16:46', '2018-09-08 15:16:46', 5, 'images/default_product_image.jpg'),
(207, 'MOD-701', '', 800, 1200, 1, '2018-09-08 15:17:19', '2018-09-08 15:17:19', 5, 'images/default_product_image.jpg'),
(208, 'MOD-8013', '', 800, 1350, 1, '2018-09-08 15:17:49', '2018-09-08 15:17:49', 5, 'images/default_product_image.jpg'),
(209, 'MOD-838-596', '', 900, 1530, 1, '2018-09-08 15:21:41', '2018-09-08 15:21:41', 5, 'images/default_product_image.jpg'),
(210, 'MOD-8809', '', 450, 790, 1, '2018-09-08 15:22:15', '2018-09-08 15:22:15', 5, 'images/default_product_image.jpg'),
(211, 'MOD-A6368', '', 700, 1150, 1, '2018-09-08 15:22:39', '2018-09-08 15:22:39', 5, 'images/default_product_image.jpg'),
(212, 'MOD-B663', '', 800, 1350, 1, '2018-09-08 15:23:11', '2018-09-08 15:23:11', 5, 'images/default_product_image.jpg'),
(213, 'MOD-F170119-4', '', 1000, 1590, 1, '2018-09-08 15:23:38', '2018-09-08 15:23:38', 5, 'images/default_product_image.jpg'),
(214, 'MOD-N862', '', 700, 1150, 1, '2018-09-08 15:24:07', '2018-09-13 19:37:31', 5, 'images/default_product_image.jpg'),
(215, 'MOD-PL556', '', 310, 560, 1, '2018-09-08 15:24:40', '2018-09-08 15:24:40', 5, 'images/default_product_image.jpg'),
(216, 'MOD-STEP1', '', 425, 950, 1, '2018-09-08 15:25:17', '2018-09-08 15:25:17', 5, 'images/default_product_image.jpg'),
(217, 'M-A1', '', 260, 500, 1, '2018-09-08 15:26:28', '2018-09-08 15:26:28', 8, 'images/default_product_image.jpg'),
(218, 'M-A2', '', 210, 450, 1, '2018-09-08 15:26:48', '2018-09-08 15:26:48', 8, 'images/default_product_image.jpg'),
(219, 'MS-07', '', 450, 790, 1, '2018-09-08 15:27:14', '2018-09-13 19:06:38', 5, 'images/default_product_image.jpg'),
(220, 'MS-10', '', 550, 890, 1, '2018-09-08 15:27:36', '2018-09-08 15:27:36', 5, 'images/default_product_image.jpg'),
(221, 'MS-303', '', 600, 950, 1, '2018-09-08 15:28:00', '2018-09-08 15:28:00', 5, 'images/default_product_image.jpg'),
(222, 'MS-99', '', 900, 1390, 1, '2018-09-08 15:28:38', '2018-09-08 15:28:38', 5, 'images/default_product_image.jpg'),
(223, 'NE-Big Rose-42', '', 350, 460, 1, '2018-09-08 15:29:32', '2018-09-08 15:31:22', 10, 'images/default_product_image.jpg'),
(224, 'NE-Carpet-1', '', 4700, 6990, 1, '2018-09-08 15:30:51', '2018-09-08 15:30:51', 10, 'images/default_product_image.jpg'),
(225, 'NE-Carpet-16', '', 3000, 4490, 1, '2018-09-08 15:32:16', '2018-09-08 15:32:16', 10, 'images/default_product_image.jpg'),
(226, 'NE-Carpet-17', '', 2500, 3750, 1, '2018-09-08 15:32:50', '2018-09-08 15:32:50', 10, 'images/default_product_image.jpg'),
(227, 'NE-Carpet-18', '', 1400, 2090, 1, '2018-09-08 15:33:19', '2018-09-08 15:33:19', 10, 'images/default_product_image.jpg'),
(228, 'NE-Carpet-2', '', 2700, 3990, 1, '2018-09-08 15:33:51', '2018-09-08 15:33:51', 10, 'images/default_product_image.jpg'),
(229, 'NE-Carpet-21', '', 1700, 2550, 1, '2018-09-08 15:34:16', '2018-09-08 15:34:16', 10, 'images/default_product_image.jpg'),
(230, 'NE-Carpet-22', '', 2900, 4350, 1, '2018-09-08 15:34:45', '2018-09-08 15:34:45', 10, 'images/default_product_image.jpg'),
(231, 'NE-Carpet-23', '', 4200, 6300, 1, '2018-09-08 15:35:12', '2018-09-08 15:35:12', 10, 'images/default_product_image.jpg'),
(232, 'NE-Carpet-3', '', 1500, 2250, 1, '2018-09-08 15:35:40', '2018-09-08 15:35:40', 10, 'images/default_product_image.jpg'),
(233, 'NE-Cloth stic-39', '', 75, 95, 1, '2018-09-08 15:37:02', '2018-09-08 15:37:02', 10, 'images/default_product_image.jpg'),
(234, 'NE-Cloth flower-44', '', 80, 130, 1, '2018-09-08 15:37:54', '2018-09-14 16:18:12', 10, 'images/default_product_image.jpg'),
(235, 'NE-Cocks2-52', '', 6200, 9750, 1, '2018-09-08 15:38:41', '2018-09-08 15:38:41', 10, 'images/default_product_image.jpg'),
(236, 'NE-Cranes-48', '', 3000, 4490, 1, '2018-09-08 15:39:19', '2018-09-08 15:39:19', 10, 'images/default_product_image.jpg'),
(237, 'NE-Craqnes2-49', '', 3300, 4950, 1, '2018-09-08 15:40:26', '2018-09-08 15:40:26', 10, 'images/default_product_image.jpg'),
(238, 'NE-Deer-59', '', 5500, 8250, 1, '2018-09-08 15:41:01', '2018-09-08 15:41:01', 10, 'images/default_product_image.jpg'),
(239, 'NE-Dry Flower-34', '', 170, 250, 1, '2018-09-08 15:41:51', '2018-09-08 15:41:51', 10, 'images/default_product_image.jpg'),
(240, 'NE-Fiber-46', '', 2600, 3890, 1, '2018-09-08 15:42:29', '2018-09-08 15:42:29', 10, 'images/default_product_image.jpg'),
(241, 'NE-Flower Tob -30', '', 300, 450, 1, '2018-09-08 15:54:25', '2018-09-08 15:54:25', 10, 'images/default_product_image.jpg'),
(242, 'NE-Flower Tob -31', '', 1000, 1290, 1, '2018-09-08 15:54:53', '2018-09-08 15:54:53', 10, 'images/default_product_image.jpg'),
(243, 'NE-Fruit-32', '', 250, 320, 1, '2018-09-08 15:55:48', '2018-09-08 15:55:48', 10, 'images/default_product_image.jpg'),
(244, 'NE-Fuldani-26', '', 1300, 1790, 1, '2018-09-08 15:56:29', '2018-09-08 15:56:29', 10, 'images/default_product_image.jpg'),
(245, 'NE-Fuldani-27', '', 1300, 1790, 1, '2018-09-08 15:57:06', '2018-09-08 15:57:06', 10, 'images/default_product_image.jpg'),
(246, 'NE-Fuldani-28', '', 600, 890, 1, '2018-09-08 15:57:37', '2018-09-08 15:57:37', 10, 'images/default_product_image.jpg'),
(247, 'NE-Fuldani-29', '', 2300, 3450, 1, '2018-09-08 15:58:03', '2018-09-08 15:58:03', 10, 'images/default_product_image.jpg'),
(248, 'NE-Lemp-33', '', 4000, 5990, 1, '2018-09-08 15:59:12', '2018-09-08 15:59:12', 10, 'images/default_product_image.jpg'),
(249, 'NE-Leaf-40', '', 200, 260, 1, '2018-09-08 15:59:54', '2018-09-08 15:59:54', 10, 'images/default_product_image.jpg'),
(250, 'NE-Leaf-54', '', 2600, 3890, 1, '2018-09-08 16:01:27', '2018-09-08 16:01:27', 10, 'images/default_product_image.jpg'),
(251, 'NE-Leaf-55', '', 2500, 3750, 1, '2018-09-08 16:01:56', '2018-09-08 16:01:56', 10, 'images/default_product_image.jpg'),
(252, 'NE-Met-12', '', 700, 1050, 1, '2018-09-08 16:02:29', '2018-09-08 16:02:29', 10, 'images/default_product_image.jpg'),
(253, 'NE-Met-13', '', 500, 750, 1, '2018-09-08 16:02:53', '2018-09-08 16:02:53', 10, 'images/default_product_image.jpg'),
(254, 'NE-Outwer Met-14', '', 400, 590, 1, '2018-09-08 16:03:38', '2018-09-08 16:03:38', 10, 'images/default_product_image.jpg'),
(255, 'NE-Papush-10', '', 350, 590, 1, '2018-09-08 16:04:09', '2018-09-08 16:04:09', 10, 'images/default_product_image.jpg'),
(256, 'NE-Papush-11', '', 180, 350, 1, '2018-09-08 16:04:32', '2018-09-08 16:04:32', 10, 'images/default_product_image.jpg'),
(257, 'NE-Papush-5', '', 350, 590, 1, '2018-09-08 16:04:59', '2018-09-08 16:04:59', 10, 'images/default_product_image.jpg'),
(258, 'NE-Papush-6', '', 650, 1050, 1, '2018-09-08 16:05:25', '2018-09-08 16:05:25', 10, 'images/default_product_image.jpg'),
(259, 'NE-Papush-8', '', 1300, 1950, 1, '2018-09-08 16:05:56', '2018-09-08 16:05:56', 10, 'images/default_product_image.jpg'),
(260, 'NE-Papush-9', '', 300, 590, 1, '2018-09-08 16:06:22', '2018-09-08 16:06:22', 10, 'images/default_product_image.jpg'),
(261, 'NE-Pigeon-58', '', 4100, 6150, 1, '2018-09-08 16:07:09', '2018-09-08 16:07:09', 10, 'images/default_product_image.jpg'),
(262, 'NE-Pigeon-50', '', 3500, 5250, 1, '2018-09-08 16:07:43', '2018-09-08 16:07:43', 10, 'images/default_product_image.jpg'),
(263, 'NE-Pigeon-56', '', 4500, 6750, 1, '2018-09-08 16:08:11', '2018-09-08 16:08:11', 10, 'images/default_product_image.jpg'),
(264, 'NE-Pineapple-43', '', 500, 620, 1, '2018-09-08 16:08:56', '2018-09-08 16:08:56', 10, 'images/default_product_image.jpg'),
(265, 'NE-Plastic Met-25', '', 65, 120, 1, '2018-09-08 16:09:52', '2018-09-08 16:09:52', 10, 'images/default_product_image.jpg'),
(266, 'NE-Real Flower-36', '', 190, 260, 1, '2018-09-08 16:10:44', '2018-09-08 16:10:44', 10, 'images/default_product_image.jpg'),
(267, 'NE-Real Flower-38', '', 200, 260, 1, '2018-09-08 16:11:19', '2018-09-08 16:11:19', 10, 'images/default_product_image.jpg'),
(268, 'NE-Tabel Met-24', '', 105, 190, 1, '2018-09-08 16:12:01', '2018-09-08 16:12:01', 10, 'images/default_product_image.jpg'),
(269, 'NE-Tree-35', '', 450, 550, 1, '2018-09-08 16:12:40', '2018-09-08 16:12:40', 10, 'images/default_product_image.jpg'),
(270, 'NE-Tree Flower-37', '', 40, 60, 1, '2018-09-08 16:13:37', '2018-09-08 16:13:37', 10, 'images/default_product_image.jpg'),
(271, 'NE-Tree Leaf-41', '', 150, 220, 1, '2018-09-08 16:14:32', '2018-09-08 16:14:32', 10, 'images/default_product_image.jpg'),
(272, 'Netro-2', '', 140, 260, 1, '2018-09-08 16:15:42', '2018-09-08 16:19:56', 3, 'images/default_product_image.jpg'),
(273, 'Netro-3', '', 110, 210, 1, '2018-09-08 16:16:48', '2018-09-08 16:19:52', 3, 'images/default_product_image.jpg'),
(274, 'Netro-4', '', 110, 210, 1, '2018-09-08 16:17:16', '2018-09-08 16:19:47', 3, 'images/default_product_image.jpg'),
(275, 'NS-01', '', 300, 650, 1, '2018-09-08 16:17:49', '2018-09-08 16:19:30', 5, 'images/default_product_image.jpg'),
(276, 'NS-03', '', 235, 520, 1, '2018-09-08 16:18:15', '2018-09-08 16:19:26', 5, 'images/default_product_image.jpg'),
(277, 'NS-05', '', 320, 620, 1, '2018-09-08 16:18:36', '2018-09-08 16:19:18', 5, 'images/default_product_image.jpg'),
(278, 'NS-06', '', 300, 560, 1, '2018-09-08 16:19:03', '2018-09-08 16:19:03', 5, 'images/default_product_image.jpg'),
(279, 'NS-199', '', 450, 790, 1, '2018-09-08 16:20:44', '2018-09-08 16:20:44', 5, 'images/default_product_image.jpg'),
(280, 'NS-888-1', '', 500, 850, 1, '2018-09-08 16:21:27', '2018-09-08 16:21:27', 5, 'images/default_product_image.jpg'),
(281, 'NS-Kids', '', 300, 470, 1, '2018-09-08 16:22:23', '2018-09-08 16:22:23', 5, 'images/default_product_image.jpg'),
(282, 'NV-8', '', 700, 1350, 1, '2018-09-08 16:23:31', '2018-09-08 16:23:31', 1, 'images/default_product_image.jpg'),
(283, 'PLS-1', '', 950, 1450, 1, '2018-09-08 16:24:16', '2018-09-08 16:24:16', 6, 'images/default_product_image.jpg'),
(284, 'PLS-2', '', 1280, 1980, 1, '2018-09-08 16:24:49', '2018-09-08 16:24:49', 6, 'images/default_product_image.jpg'),
(285, 'PLS-3', '', 1280, 1980, 1, '2018-09-08 16:25:11', '2018-09-08 16:25:11', 6, 'images/default_product_image.jpg'),
(286, 'PLS-4', '', 1200, 1890, 1, '2018-09-08 16:25:37', '2018-09-08 16:25:37', 6, 'images/default_product_image.jpg'),
(287, 'PL-03', '', 1100, 1750, 1, '2018-09-08 16:26:04', '2018-09-08 16:26:04', 6, 'images/default_product_image.jpg'),
(288, 'PL-04', '', 1200, 1750, 1, '2018-09-08 16:26:26', '2018-09-08 16:26:26', 6, 'images/default_product_image.jpg'),
(289, 'PL-05', '', 900, 1450, 1, '2018-09-08 16:26:56', '2018-09-08 16:26:56', 6, 'images/default_product_image.jpg'),
(290, 'PL-06', '', 1300, 1950, 1, '2018-09-08 16:27:19', '2018-09-08 16:27:19', 6, 'images/default_product_image.jpg'),
(291, 'PL-07', '', 500, 850, 1, '2018-09-08 16:27:56', '2018-09-08 16:27:56', 6, 'images/default_product_image.jpg'),
(292, 'PLS-08', '', 900, 1350, 1, '2018-09-08 16:29:57', '2018-09-08 16:29:57', 6, 'images/default_product_image.jpg'),
(293, 'PLS-011', '', 900, 1650, 1, '2018-09-08 16:30:24', '2018-09-08 16:30:24', 6, 'images/default_product_image.jpg'),
(294, 'PO-S1', '', 1050, 1350, 1, '2018-09-08 16:31:01', '2018-09-08 16:31:01', 7, 'images/default_product_image.jpg'),
(295, 'PO-S2', '', 1300, 1650, 1, '2018-09-08 16:31:21', '2018-09-08 16:31:21', 7, 'images/default_product_image.jpg'),
(296, 'PTI-HP', '', 200, 450, 1, '2018-09-08 16:32:09', '2018-09-08 16:32:09', 6, 'images/default_product_image.jpg'),
(297, 'PTI-SP13', '', 1150, 1850, 1, '2018-09-08 16:32:51', '2018-09-08 16:32:51', 6, 'images/default_product_image.jpg'),
(298, 'RA-01', '', 45, 70, 1, '2018-09-08 16:33:33', '2018-09-08 16:33:33', 6, 'images/default_product_image.jpg'),
(299, 'RA-03', '', 170, 200, 1, '2018-09-08 16:34:02', '2018-09-08 16:34:02', 6, 'images/default_product_image.jpg'),
(300, 'RA-04', '', 175, 250, 1, '2018-09-08 16:34:27', '2018-09-08 16:34:27', 6, 'images/default_product_image.jpg'),
(301, 'RA-05', '', 150, 220, 1, '2018-09-08 16:34:51', '2018-09-08 16:34:51', 6, 'images/default_product_image.jpg'),
(302, 'RA-06', '', 120, 180, 1, '2018-09-08 16:35:15', '2018-09-08 16:35:15', 6, 'images/default_product_image.jpg'),
(303, 'RA-1', '', 223, 250, 1, '2018-09-08 16:35:42', '2018-09-08 16:35:42', 6, 'images/default_product_image.jpg'),
(304, 'RCG-P2', '', 1000, 1950, 1, '2018-09-08 16:36:36', '2018-09-08 16:36:36', 1, 'images/default_product_image.jpg'),
(305, 'RG-P1', '', 550, 1050, 1, '2018-09-08 16:38:53', '2018-09-08 16:38:53', 1, 'images/default_product_image.jpg'),
(306, 'RG-1', '', 280, 490, 1, '2018-09-08 17:01:23', '2018-09-08 17:01:23', 1, 'images/default_product_image.jpg'),
(307, 'RG-2', '', 450, 670, 1, '2018-09-08 17:01:43', '2018-09-08 17:01:43', 1, 'images/default_product_image.jpg'),
(308, 'RG-4', '', 500, 890, 1, '2018-09-08 17:02:07', '2018-09-08 17:02:07', 1, 'images/default_product_image.jpg'),
(309, 'RH-04', '', 90, 170, 1, '2018-09-08 17:02:57', '2018-09-08 17:02:57', 12, 'images/default_product_image.jpg'),
(310, 'RH-06', '', 45, 100, 1, '2018-09-08 17:03:20', '2018-09-08 17:03:20', 12, 'images/default_product_image.jpg'),
(311, 'RH-08', '', 30, 80, 1, '2018-09-08 17:03:55', '2018-09-08 17:03:55', 12, 'images/default_product_image.jpg'),
(312, 'RH-11', '', 30, 80, 1, '2018-09-08 17:04:20', '2018-09-08 17:04:20', 12, 'images/default_product_image.jpg'),
(313, 'RS-1', '', 335, 580, 1, '2018-09-08 17:04:56', '2018-09-08 17:04:56', 4, 'images/default_product_image.jpg'),
(314, 'RS-19', '', 110, 200, 1, '2018-09-08 17:05:15', '2018-09-08 17:05:15', 4, 'images/default_product_image.jpg'),
(315, 'RS-21', '', 40, 90, 1, '2018-09-08 17:06:11', '2018-09-08 17:06:11', 4, 'images/default_product_image.jpg'),
(316, 'RS-22', '', 40, 90, 1, '2018-09-08 17:06:45', '2018-09-08 17:06:45', 4, 'images/default_product_image.jpg'),
(317, 'RS-28', '', 35, 90, 1, '2018-09-08 17:07:33', '2018-09-08 17:07:33', 4, 'images/default_product_image.jpg'),
(318, 'RS-31', '', 80, 170, 1, '2018-09-08 17:07:58', '2018-09-08 17:07:58', 4, 'images/default_product_image.jpg'),
(319, 'RS-32', '', 220, 350, 1, '2018-09-08 17:08:23', '2018-09-08 17:08:23', 4, 'images/default_product_image.jpg'),
(320, 'RS-33', '', 7, 20, 1, '2018-09-08 17:08:47', '2018-09-08 17:08:47', 4, 'images/default_product_image.jpg'),
(321, 'RS-35', '', 55, 100, 1, '2018-09-08 17:09:12', '2018-09-08 17:09:12', 4, 'images/default_product_image.jpg'),
(322, 'RS-39', '', 200, 350, 1, '2018-09-08 17:09:34', '2018-09-08 17:09:34', 4, 'images/default_product_image.jpg'),
(323, 'RS-4', '', 35, 80, 1, '2018-09-08 17:10:15', '2018-09-08 17:10:15', 4, 'images/default_product_image.jpg'),
(324, 'RS-41', '', 425, 660, 1, '2018-09-08 17:13:20', '2018-09-08 17:13:20', 4, 'images/default_product_image.jpg'),
(325, 'RS-42', '', 230, 490, 1, '2018-09-08 17:14:14', '2018-09-08 17:14:14', 4, 'images/default_product_image.jpg'),
(326, 'RS-44', '', 60, 120, 1, '2018-09-08 17:15:19', '2018-09-08 17:15:19', 4, 'images/default_product_image.jpg'),
(327, 'RS-47', '', 6, 20, 1, '2018-09-08 17:15:58', '2018-09-08 17:15:58', 4, 'images/default_product_image.jpg'),
(328, 'RS-57', '', 190, 300, 1, '2018-09-08 17:17:25', '2018-09-08 17:17:25', 4, 'images/default_product_image.jpg'),
(329, 'RS-65', '', 360, 580, 1, '2018-09-08 17:18:05', '2018-09-08 17:18:05', 4, 'images/default_product_image.jpg'),
(330, 'RS-66', '', 410, 660, 1, '2018-09-08 17:18:42', '2018-09-08 17:18:42', 4, 'images/default_product_image.jpg'),
(331, 'RS-67', '', 650, 1050, 1, '2018-09-08 17:19:04', '2018-09-08 17:19:04', 4, 'images/default_product_image.jpg'),
(332, 'RS-69', '', 19, 90, 1, '2018-09-08 17:19:45', '2018-09-08 17:19:45', 4, 'images/default_product_image.jpg'),
(333, 'RS-7', '', 630, 1000, 1, '2018-09-08 17:20:11', '2018-09-08 17:20:11', 4, 'images/default_product_image.jpg'),
(334, 'RS-73', '', 120, 220, 1, '2018-09-08 17:20:35', '2018-09-08 17:20:35', 4, 'images/default_product_image.jpg'),
(335, 'RS-75', '', 220, 430, 1, '2018-09-08 17:21:08', '2018-09-08 17:21:08', 4, 'images/default_product_image.jpg'),
(336, 'RS-77', '', 190, 360, 1, '2018-09-08 17:21:35', '2018-09-08 17:21:35', 4, 'images/default_product_image.jpg'),
(337, 'RS-79', '', 370, 580, 1, '2018-09-08 17:22:00', '2018-09-08 17:22:00', 4, 'images/default_product_image.jpg'),
(338, 'RS-8', '', 450, 700, 1, '2018-09-08 17:22:50', '2018-09-08 17:22:50', 4, 'images/default_product_image.jpg'),
(339, 'RS-80', '', 275, 450, 1, '2018-09-08 17:23:11', '2018-09-08 17:23:11', 4, 'images/default_product_image.jpg'),
(340, 'RS-89', '', 220, 360, 1, '2018-09-08 17:23:53', '2018-09-08 17:23:53', 4, 'images/default_product_image.jpg'),
(341, 'RS-9', '', 120, 380, 1, '2018-09-08 17:24:14', '2018-09-08 17:24:14', 4, 'images/default_product_image.jpg'),
(342, 'RS-90', '', 220, 400, 1, '2018-09-08 17:24:41', '2018-09-08 17:24:41', 4, 'images/default_product_image.jpg'),
(343, 'RS-92', '', 130, 220, 1, '2018-09-08 17:25:09', '2018-09-08 17:25:09', 4, 'images/default_product_image.jpg'),
(344, 'RS-93', '', 270, 430, 1, '2018-09-08 17:25:29', '2018-09-08 17:25:29', 4, 'images/default_product_image.jpg'),
(345, 'SGC-100', '', 230, 440, 1, '2018-09-08 17:27:32', '2018-09-08 17:27:32', 4, 'images/default_product_image.jpg'),
(346, 'SGC-102', '', 210, 390, 1, '2018-09-08 17:28:10', '2018-09-08 17:28:10', 4, 'images/default_product_image.jpg'),
(347, 'SGC-103', '', 210, 390, 1, '2018-09-08 17:28:40', '2018-09-08 17:28:40', 4, 'images/default_product_image.jpg'),
(348, 'SGC-16', '', 70, 160, 1, '2018-09-08 17:29:08', '2018-09-08 17:29:08', 4, 'images/default_product_image.jpg'),
(349, 'SGC-18', '', 210, 350, 1, '2018-09-08 17:29:44', '2018-09-08 17:29:44', 4, 'images/default_product_image.jpg'),
(350, 'SGC-20', '', 250, 450, 1, '2018-09-08 17:30:11', '2018-09-08 17:30:11', 4, 'images/default_product_image.jpg'),
(351, 'SGC-21', '', 130, 220, 1, '2018-09-08 17:30:45', '2018-09-08 17:30:45', 4, 'images/default_product_image.jpg'),
(352, 'SGC-26', '', 130, 250, 1, '2018-09-08 17:31:16', '2018-09-08 17:31:16', 4, 'images/default_product_image.jpg'),
(353, 'SGC-33', '', 130, 220, 1, '2018-09-08 17:31:45', '2018-09-08 17:31:45', 4, 'images/default_product_image.jpg'),
(354, 'SGC-34', '', 90, 180, 1, '2018-09-08 17:32:17', '2018-09-08 17:32:17', 4, 'images/default_product_image.jpg'),
(355, 'SGC-44', '', 480, 800, 1, '2018-09-08 17:32:59', '2018-09-08 17:32:59', 4, 'images/default_product_image.jpg'),
(356, 'SGC-5', '', 2400, 3600, 1, '2018-09-08 17:33:33', '2018-09-08 17:33:33', 4, 'images/default_product_image.jpg'),
(357, 'SGC-50', '', 500, 950, 1, '2018-09-08 17:34:31', '2018-09-08 17:34:31', 4, 'images/default_product_image.jpg'),
(358, 'SGC-58', '', 520, 880, 1, '2018-09-08 17:35:05', '2018-09-08 17:35:05', 4, 'images/default_product_image.jpg'),
(359, 'SGC-59', '', 600, 900, 1, '2018-09-08 17:35:28', '2018-09-08 17:35:28', 4, 'images/default_product_image.jpg'),
(360, 'SGC-6', '', 1950, 3100, 1, '2018-09-08 17:35:51', '2018-09-08 17:35:51', 4, 'images/default_product_image.jpg'),
(361, 'SGC-60', '', 650, 950, 1, '2018-09-08 17:36:11', '2018-09-08 17:36:11', 4, 'images/default_product_image.jpg'),
(362, 'SGC-63', '', 1100, 1650, 1, '2018-09-08 17:36:32', '2018-09-08 17:36:32', 4, 'images/default_product_image.jpg'),
(363, 'SGC-7', '', 1700, 2700, 1, '2018-09-08 17:36:52', '2018-09-08 17:36:52', 4, 'images/default_product_image.jpg'),
(364, 'SGC-78', '', 80, 150, 1, '2018-09-08 17:37:12', '2018-09-08 17:37:12', 4, 'images/default_product_image.jpg'),
(365, 'SGC-8', '', 1400, 2300, 1, '2018-09-08 17:37:34', '2018-09-08 17:37:34', 4, 'images/default_product_image.jpg'),
(366, 'SGC-91', '', 560, 950, 1, '2018-09-08 17:38:01', '2018-09-08 17:38:01', 4, 'images/default_product_image.jpg'),
(367, 'SGC-93', '', 100, 180, 1, '2018-09-08 17:38:21', '2018-09-08 17:38:21', 4, 'images/default_product_image.jpg'),
(368, 'SGC-96', '', 140, 250, 1, '2018-09-08 17:38:43', '2018-09-08 17:38:43', 4, 'images/default_product_image.jpg'),
(369, 'SGC-97', '', 150, 250, 1, '2018-09-08 17:39:20', '2018-09-08 17:39:20', 4, 'images/default_product_image.jpg'),
(370, 'SGC-98', '', 200, 390, 1, '2018-09-08 17:40:38', '2018-09-08 17:40:38', 4, 'images/default_product_image.jpg'),
(371, 'SGC-99', '', 230, 450, 1, '2018-09-08 17:41:12', '2018-09-08 17:41:12', 4, 'images/default_product_image.jpg'),
(372, 'SH-5526', '', 1300, 2650, 1, '2018-09-08 17:42:09', '2018-09-08 17:42:09', 5, 'images/default_product_image.jpg'),
(373, 'SH-5781', '', 1040, 1680, 1, '2018-09-08 17:42:48', '2018-09-08 17:42:48', 5, 'images/default_product_image.jpg'),
(374, 'SS-125', '', 450, 750, 1, '2018-09-08 17:43:42', '2018-09-08 17:43:42', 5, 'images/default_product_image.jpg'),
(375, 'SS-1511', '', 500, 840, 1, '2018-09-08 17:44:10', '2018-09-08 17:44:10', 5, 'images/default_product_image.jpg'),
(376, 'SS-1326', '', 370, 600, 1, '2018-09-08 17:44:34', '2018-09-08 17:44:34', 5, 'images/default_product_image.jpg'),
(377, 'SS-01150-6', '', 900, 1450, 1, '2018-09-08 17:45:05', '2018-09-08 17:45:05', 5, 'images/default_product_image.jpg'),
(378, 'SS-0855', '', 2100, 3150, 1, '2018-09-08 17:45:33', '2018-09-08 17:45:33', 5, 'images/default_product_image.jpg'),
(379, 'SS-1028', '', 480, 800, 1, '2018-09-08 17:45:55', '2018-09-08 17:45:55', 5, 'images/default_product_image.jpg'),
(380, 'SS-1028-2', '', 500, 850, 1, '2018-09-08 17:46:17', '2018-09-08 17:46:17', 5, 'images/default_product_image.jpg'),
(381, 'SS-10855', '', 2150, 3250, 1, '2018-09-08 17:47:45', '2018-09-08 17:47:45', 5, 'images/default_product_image.jpg'),
(382, 'SS-1164', '', 450, 750, 1, '2018-09-08 17:48:04', '2018-09-08 17:48:04', 5, 'images/default_product_image.jpg'),
(383, 'SS-131', '', 950, 1590, 1, '2018-09-08 17:48:31', '2018-09-08 17:48:31', 5, 'images/default_product_image.jpg'),
(384, 'SS-131-4', '', 700, 1050, 1, '2018-09-08 17:48:59', '2018-09-08 17:48:59', 5, 'images/default_product_image.jpg'),
(385, 'SS-137', '', 950, 1590, 1, '2018-09-08 17:49:31', '2018-09-09 15:27:48', 5, 'images/default_product_image.jpg'),
(386, 'SS-1389', '', 750, 1090, 1, '2018-09-08 17:49:51', '2018-09-08 17:50:21', 5, 'images/default_product_image.jpg'),
(387, 'SS-1563', '', 750, 1290, 1, '2018-09-08 17:51:11', '2018-09-08 17:51:11', 5, 'images/default_product_image.jpg'),
(388, 'SS-1650', '', 1150, 1850, 1, '2018-09-08 18:06:42', '2018-09-08 18:06:42', 4, 'images/default_product_image.jpg'),
(389, 'SS-1656', '', 1200, 1950, 1, '2018-09-09 15:12:01', '2018-09-09 15:15:33', 5, 'images/default_product_image.jpg'),
(390, 'SS-1702', '', 420, 720, 1, '2018-09-09 15:13:55', '2018-09-09 15:15:45', 5, 'images/default_product_image.jpg'),
(391, 'SS-1788', '', 700, 1050, 1, '2018-09-09 15:14:28', '2018-09-12 17:55:56', 5, 'images/default_product_image.jpg'),
(392, 'SS-1790', '', 520, 890, 1, '2018-09-09 15:14:55', '2018-09-09 15:16:05', 5, 'images/default_product_image.jpg'),
(393, 'SS-180-1', '', 1200, 1850, 1, '2018-09-09 15:33:31', '2018-09-09 15:33:31', 5, 'images/default_product_image.jpg'),
(394, 'SS-182', '', 420, 720, 1, '2018-09-09 15:34:10', '2018-09-09 15:34:10', 5, 'images/default_product_image.jpg'),
(395, 'SS-18812', '', 750, 1190, 1, '2018-09-09 15:34:34', '2018-09-09 15:34:34', 5, 'images/default_product_image.jpg'),
(396, 'SS-18816', '', 750, 1190, 1, '2018-09-09 15:34:56', '2018-09-09 15:34:56', 5, 'images/default_product_image.jpg'),
(397, 'SS-50139', '', 1400, 2100, 1, '2018-09-09 15:35:27', '2018-09-09 15:35:27', 5, 'images/default_product_image.jpg'),
(398, 'SS-200-7', '', 2150, 3250, 1, '2018-09-09 15:35:53', '2018-09-09 15:35:53', 5, 'images/default_product_image.jpg'),
(399, 'SS-20162', '', 2650, 3990, 1, '2018-09-09 15:43:09', '2018-09-09 15:43:09', 5, 'images/default_product_image.jpg'),
(400, 'SS-2060', '', 1000, 1650, 1, '2018-09-09 15:43:30', '2018-09-09 15:43:30', 5, 'images/default_product_image.jpg'),
(401, 'SS-207', '', 1100, 1800, 1, '2018-09-09 15:43:57', '2018-09-09 15:43:57', 5, 'images/default_product_image.jpg'),
(402, 'SS-2205', '', 750, 1200, 1, '2018-09-09 15:44:20', '2018-09-09 15:44:20', 5, 'images/default_product_image.jpg'),
(403, 'SS-2220', '', 750, 1190, 1, '2018-09-09 15:44:45', '2018-09-09 15:44:45', 5, 'images/default_product_image.jpg'),
(404, 'SS-25978-7', '', 800, 1350, 1, '2018-09-09 15:45:16', '2018-09-09 15:45:16', 5, 'images/default_product_image.jpg'),
(405, 'SS-2629-836', '', 750, 1190, 1, '2018-09-09 15:45:52', '2018-09-09 15:45:52', 5, 'images/default_product_image.jpg'),
(406, 'SS-28-00159', '', 2500, 3450, 1, '2018-09-09 15:48:14', '2018-09-09 15:48:14', 5, 'images/default_product_image.jpg'),
(407, 'SS-287-24', '', 700, 1050, 1, '2018-09-09 15:48:41', '2018-09-09 15:48:41', 5, 'images/default_product_image.jpg'),
(408, 'SS-2881-6', '', 500, 890, 1, '2018-09-09 15:51:16', '2018-09-09 15:51:16', 5, 'images/default_product_image.jpg'),
(409, 'SS-2978-2', '', 850, 1280, 1, '2018-09-09 15:51:46', '2018-09-09 15:51:46', 5, 'images/default_product_image.jpg'),
(410, 'SS-299-4', '', 700, 1050, 1, '2018-09-09 15:53:10', '2018-09-12 17:56:25', 5, 'images/default_product_image.jpg'),
(411, 'SS-299-6', '', 700, 1050, 1, '2018-09-09 15:53:34', '2018-09-09 15:53:34', 5, 'images/default_product_image.jpg'),
(412, 'SS-3002', '', 650, 950, 1, '2018-09-09 15:53:56', '2018-09-09 15:53:56', 5, 'images/default_product_image.jpg'),
(413, 'SS-318-3', '', 500, 890, 1, '2018-09-09 15:54:19', '2018-09-09 15:54:19', 5, 'images/default_product_image.jpg'),
(414, 'SS-318-31', '', 325, 600, 1, '2018-09-09 15:54:45', '2018-09-09 15:54:45', 5, 'images/default_product_image.jpg'),
(415, 'SS-318-52', '', 600, 960, 1, '2018-09-09 15:55:20', '2018-09-09 15:55:20', 5, 'images/default_product_image.jpg'),
(416, 'SS-318-9', '', 480, 800, 1, '2018-09-09 15:55:43', '2018-09-09 15:55:43', 5, 'images/default_product_image.jpg'),
(417, 'SS-320', '', 325, 600, 1, '2018-09-09 15:56:19', '2018-09-09 15:56:19', 5, 'images/default_product_image.jpg'),
(418, 'SS-321', '', 420, 720, 1, '2018-09-09 15:56:38', '2018-09-09 15:56:38', 5, 'images/default_product_image.jpg'),
(419, 'SS-322', '', 420, 720, 1, '2018-09-09 15:57:02', '2018-09-09 15:57:02', 5, 'images/default_product_image.jpg'),
(420, 'SS-3385', '', 750, 1190, 1, '2018-09-09 15:57:26', '2018-09-09 15:57:26', 5, 'images/default_product_image.jpg');
INSERT INTO `product` (`product_id`, `title`, `description`, `unit_price`, `sale_price`, `active`, `created`, `edited`, `category_id`, `image_url`) VALUES
(421, 'SS-3392', '', 750, 1190, 1, '2018-09-09 15:57:59', '2018-09-09 15:57:59', 5, 'images/default_product_image.jpg'),
(422, 'SS-3617', '', 750, 1190, 1, '2018-09-09 15:59:25', '2018-09-09 15:59:25', 5, 'images/default_product_image.jpg'),
(423, 'SS-3619', '', 750, 1190, 1, '2018-09-09 15:59:52', '2018-09-09 15:59:52', 5, 'images/default_product_image.jpg'),
(424, 'SS-3626', '', 750, 1190, 1, '2018-09-09 16:00:20', '2018-09-09 16:00:20', 5, 'images/default_product_image.jpg'),
(425, 'SS-3629', '', 750, 1190, 1, '2018-09-09 16:00:44', '2018-09-09 16:00:44', 5, 'images/default_product_image.jpg'),
(426, 'SS-3723-18', '', 600, 950, 1, '2018-09-09 16:01:10', '2018-09-09 16:01:10', 5, 'images/default_product_image.jpg'),
(427, 'SS-389-662', '', 700, 1050, 1, '2018-09-09 16:01:42', '2018-09-09 16:01:42', 5, 'images/default_product_image.jpg'),
(428, 'SS-389-737', '', 700, 1050, 1, '2018-09-09 16:02:07', '2018-09-09 16:02:07', 5, 'images/default_product_image.jpg'),
(429, 'SS-4108', '', 950, 1590, 1, '2018-09-09 16:02:47', '2018-09-09 16:02:47', 5, 'images/default_product_image.jpg'),
(430, 'SS-4114', '', 950, 1590, 1, '2018-09-09 16:03:23', '2018-09-09 16:03:23', 5, 'images/default_product_image.jpg'),
(431, 'SS-502', '', 500, 850, 1, '2018-09-09 16:04:08', '2018-09-09 16:04:08', 5, 'images/default_product_image.jpg'),
(432, 'SS-5262', '', 1750, 2600, 1, '2018-09-09 16:04:32', '2018-09-09 16:04:32', 5, 'images/default_product_image.jpg'),
(433, 'SS-5661', '', 1500, 2250, 1, '2018-09-09 16:05:24', '2018-09-09 16:05:24', 5, 'images/default_product_image.jpg'),
(434, 'SS-5669', '', 1500, 2290, 1, '2018-09-09 16:05:48', '2018-09-09 16:05:48', 5, 'images/default_product_image.jpg'),
(435, 'SS-602', '', 450, 720, 1, '2018-09-09 16:07:02', '2018-09-09 16:07:02', 5, 'images/default_product_image.jpg'),
(436, 'SS-605', '', 450, 800, 1, '2018-09-09 16:07:52', '2018-09-09 16:07:52', 5, 'images/default_product_image.jpg'),
(437, 'SS-608', '', 500, 880, 1, '2018-09-09 16:08:15', '2018-09-09 16:08:15', 5, 'images/default_product_image.jpg'),
(438, 'SS-6109-12', '', 800, 1280, 1, '2018-09-09 16:08:43', '2018-09-09 16:08:43', 5, 'images/default_product_image.jpg'),
(439, 'SS-6113-31', '', 800, 1280, 1, '2018-09-09 16:09:08', '2018-09-09 16:09:08', 5, 'images/default_product_image.jpg'),
(440, 'SS-625', '', 500, 880, 1, '2018-09-09 16:09:28', '2018-09-09 16:09:28', 5, 'images/default_product_image.jpg'),
(441, 'SS-628-298', '', 700, 1090, 1, '2018-09-09 16:10:00', '2018-09-09 16:10:00', 5, 'images/default_product_image.jpg'),
(442, 'SS-628-329', '', 750, 1190, 1, '2018-09-09 16:10:29', '2018-09-09 16:10:29', 5, 'images/default_product_image.jpg'),
(443, 'SS-66856', '', 520, 950, 1, '2018-09-09 16:11:09', '2018-09-09 16:11:09', 5, 'images/default_product_image.jpg'),
(444, 'SS-701', '', 400, 650, 1, '2018-09-09 16:11:32', '2018-09-09 16:11:32', 5, 'images/default_product_image.jpg'),
(445, 'SS-7288', '', 850, 1350, 1, '2018-09-09 16:11:58', '2018-09-09 16:11:58', 5, 'images/default_product_image.jpg'),
(446, 'SS-730', '', 950, 1590, 1, '2018-09-09 16:12:21', '2018-09-09 16:12:21', 5, 'images/default_product_image.jpg'),
(447, 'SS-803', '', 400, 750, 1, '2018-09-09 16:12:47', '2018-09-09 16:12:47', 5, 'images/default_product_image.jpg'),
(448, 'SS-805', '', 350, 690, 1, '2018-09-09 16:13:10', '2018-09-09 16:13:10', 5, 'images/default_product_image.jpg'),
(449, 'SS-808-16', '', 800, 1280, 1, '2018-09-09 16:13:30', '2018-09-09 16:13:30', 5, 'images/default_product_image.jpg'),
(450, 'SS-809', '', 450, 720, 1, '2018-09-09 16:14:02', '2018-09-09 16:14:02', 5, 'images/default_product_image.jpg'),
(451, 'SS-815', '', 700, 980, 1, '2018-09-09 16:14:23', '2018-09-09 16:14:23', 5, 'images/default_product_image.jpg'),
(452, 'SS-818-1', '', 700, 1050, 1, '2018-09-09 16:14:51', '2018-09-09 16:14:51', 5, 'images/default_product_image.jpg'),
(453, 'SS-823', '', 500, 850, 1, '2018-09-09 16:15:18', '2018-09-09 16:15:18', 5, 'images/default_product_image.jpg'),
(454, 'SS-8252', '', 500, 850, 1, '2018-09-09 16:15:44', '2018-09-09 16:15:44', 5, 'images/default_product_image.jpg'),
(455, 'SS-827', '', 500, 850, 1, '2018-09-09 16:16:20', '2018-09-09 16:16:20', 5, 'images/default_product_image.jpg'),
(456, 'SS-831', '', 550, 990, 1, '2018-09-09 16:16:43', '2018-09-09 16:16:43', 5, 'images/default_product_image.jpg'),
(457, 'SS-836', '', 750, 1120, 1, '2018-09-09 16:17:18', '2018-09-09 16:17:18', 5, 'images/default_product_image.jpg'),
(458, 'SS-8562', '', 800, 1280, 1, '2018-09-09 16:17:42', '2018-09-09 16:17:42', 5, 'images/default_product_image.jpg'),
(459, 'SS-8595', '', 900, 1450, 1, '2018-09-09 16:18:19', '2018-09-09 16:18:19', 5, 'images/default_product_image.jpg'),
(460, 'SS-881', '', 380, 750, 1, '2018-09-09 16:18:46', '2018-09-09 16:18:46', 5, 'images/default_product_image.jpg'),
(461, 'SS-88373', '', 1850, 2850, 1, '2018-09-09 16:19:34', '2018-09-09 16:19:34', 5, 'images/default_product_image.jpg'),
(462, 'SS-888', '', 480, 800, 1, '2018-09-09 16:54:48', '2018-09-09 16:54:48', 5, 'images/default_product_image.jpg'),
(463, 'SS-898', '', 500, 890, 1, '2018-09-09 16:55:19', '2018-09-09 16:55:19', 5, 'images/default_product_image.jpg'),
(464, 'SS-905', '', 500, 890, 1, '2018-09-09 16:56:22', '2018-09-09 16:56:22', 5, 'images/default_product_image.jpg'),
(465, 'SS-909', '', 500, 850, 1, '2018-09-09 16:56:52', '2018-09-09 16:56:52', 5, 'images/default_product_image.jpg'),
(466, 'SS-925', '', 650, 980, 1, '2018-09-09 16:57:38', '2018-09-09 16:57:38', 5, 'images/default_product_image.jpg'),
(467, 'SS-930', '', 600, 950, 1, '2018-09-09 16:58:10', '2018-09-09 16:58:10', 5, 'images/default_product_image.jpg'),
(468, 'SS-938', '', 370, 600, 1, '2018-09-09 16:58:29', '2018-09-09 16:58:29', 5, 'images/default_product_image.jpg'),
(469, 'SS-963', '', 900, 1450, 1, '2018-09-09 16:59:25', '2018-09-09 16:59:25', 5, 'images/default_product_image.jpg'),
(470, 'SS-9660', '', 2000, 2970, 1, '2018-09-09 17:02:27', '2018-09-09 17:02:27', 5, 'images/default_product_image.jpg'),
(471, 'SS-1312', '', 400, 680, 1, '2018-09-09 17:02:53', '2018-09-09 17:02:53', 5, 'images/default_product_image.jpg'),
(472, 'SS-1520', '', 450, 750, 1, '2018-09-09 17:03:14', '2018-09-09 17:03:14', 5, 'images/default_product_image.jpg'),
(473, 'SS-993', '', 600, 980, 1, '2018-09-09 17:03:43', '2018-09-09 17:03:43', 5, 'images/default_product_image.jpg'),
(474, 'SS-A08', '', 700, 1150, 1, '2018-09-09 17:09:34', '2018-09-09 17:09:34', 5, 'images/default_product_image.jpg'),
(475, 'SS-A09', '', 700, 1150, 1, '2018-09-09 17:10:01', '2018-09-09 17:10:01', 5, 'images/default_product_image.jpg'),
(476, 'SS-A201', '', 400, 680, 1, '2018-09-09 17:16:12', '2018-09-09 17:16:12', 5, 'images/default_product_image.jpg'),
(477, 'SS-A810', '', 480, 790, 1, '2018-09-09 17:17:31', '2018-09-09 17:17:31', 5, 'images/default_product_image.jpg'),
(478, 'SS-A-5', '', 450, 720, 1, '2018-09-09 17:17:52', '2018-09-09 17:17:52', 5, 'images/default_product_image.jpg'),
(479, 'SS-A005', '', 500, 850, 1, '2018-09-09 17:23:06', '2018-09-09 17:23:06', 5, 'images/default_product_image.jpg'),
(480, 'SS-A01', '', 450, 720, 1, '2018-09-09 17:23:29', '2018-09-09 17:23:29', 5, 'images/default_product_image.jpg'),
(481, 'SS-A12', '', 500, 850, 1, '2018-09-09 17:23:51', '2018-09-09 17:23:51', 5, 'images/default_product_image.jpg'),
(482, 'SS-A15', '', 500, 850, 1, '2018-09-09 17:24:14', '2018-09-09 17:24:14', 5, 'images/default_product_image.jpg'),
(483, 'SS-A302', '', 500, 850, 1, '2018-09-09 17:24:41', '2018-09-09 17:24:41', 5, 'images/default_product_image.jpg'),
(484, 'SS-A526-4', '', 750, 1190, 1, '2018-09-09 17:25:08', '2018-09-09 17:25:08', 5, 'images/default_product_image.jpg'),
(485, 'SS-A73-21', '', 700, 1050, 1, '2018-09-09 17:25:38', '2018-09-09 17:25:38', 5, 'images/default_product_image.jpg'),
(486, 'SS-A73-5', '', 700, 1050, 1, '2018-09-09 17:26:11', '2018-09-09 17:26:11', 5, 'images/default_product_image.jpg'),
(487, 'SS-A805', '', 370, 650, 1, '2018-09-09 17:29:54', '2018-09-09 17:29:54', 5, 'images/default_product_image.jpg'),
(488, 'SS-A860', '', 450, 720, 1, '2018-09-09 17:30:17', '2018-09-09 17:30:17', 5, 'images/default_product_image.jpg'),
(489, 'SS-A881', '', 700, 1120, 1, '2018-09-09 17:30:45', '2018-09-09 17:30:45', 5, 'images/default_product_image.jpg'),
(490, 'SS-A938', '', 450, 780, 1, '2018-09-09 17:31:29', '2018-09-09 17:31:29', 5, 'images/default_product_image.jpg'),
(491, 'SS-A976', '', 370, 650, 1, '2018-09-09 17:32:30', '2018-09-09 17:32:30', 5, 'images/default_product_image.jpg'),
(492, 'SS-B710', '', 420, 720, 1, '2018-09-09 17:32:57', '2018-09-09 17:32:57', 5, 'images/default_product_image.jpg'),
(493, 'SS-B712', '', 480, 800, 1, '2018-09-09 17:33:19', '2018-09-09 17:33:19', 5, 'images/default_product_image.jpg'),
(494, 'SS-B-11', '', 470, 790, 1, '2018-09-09 17:33:49', '2018-09-09 17:33:49', 5, 'images/default_product_image.jpg'),
(495, 'SS-B-2', '', 520, 880, 1, '2018-09-09 17:34:14', '2018-09-09 17:34:14', 5, 'images/default_product_image.jpg'),
(496, 'SS-B-51', '', 450, 790, 1, '2018-09-09 17:34:39', '2018-09-09 17:34:39', 5, 'images/default_product_image.jpg'),
(497, 'SS-B006', '', 520, 980, 1, '2018-09-09 17:35:03', '2018-09-09 17:35:03', 5, 'images/default_product_image.jpg'),
(498, 'SS-B1591', '', 950, 1550, 1, '2018-09-09 17:35:29', '2018-09-09 17:35:29', 5, 'images/default_product_image.jpg'),
(499, 'SS-B1901', '', 400, 650, 1, '2018-09-09 17:35:56', '2018-09-09 17:35:56', 5, 'images/default_product_image.jpg'),
(500, 'SS-B2', '', 400, 650, 1, '2018-09-09 17:36:19', '2018-09-09 17:36:19', 5, 'images/default_product_image.jpg'),
(501, 'SS-B206', '', 500, 850, 1, '2018-09-09 17:36:45', '2018-09-09 17:36:45', 5, 'images/default_product_image.jpg'),
(502, 'SS-B207', '', 500, 850, 1, '2018-09-09 17:37:17', '2018-09-09 17:37:17', 5, 'images/default_product_image.jpg'),
(503, 'SS-B713', '', 600, 950, 1, '2018-09-09 17:37:51', '2018-09-09 17:37:51', 5, 'images/default_product_image.jpg'),
(504, 'SS-B714', '', 400, 650, 1, '2018-09-09 17:38:11', '2018-09-09 17:38:11', 5, 'images/default_product_image.jpg'),
(505, 'SS-C7206', '', 500, 880, 1, '2018-09-09 17:38:56', '2018-09-09 17:38:56', 5, 'images/default_product_image.jpg'),
(506, 'SS-C7213', '', 750, 1120, 1, '2018-09-09 17:39:24', '2018-09-09 17:40:14', 5, 'images/default_product_image.jpg'),
(507, 'SS-C205', '', 450, 720, 1, '2018-09-09 17:39:52', '2018-09-09 17:39:52', 5, 'images/default_product_image.jpg'),
(508, 'SS-D886', '', 420, 690, 1, '2018-09-09 17:46:35', '2018-09-09 17:46:35', 5, 'images/default_product_image.jpg'),
(509, 'SS-E2190', '', 1000, 1650, 1, '2018-09-09 17:47:56', '2018-09-09 17:47:56', 5, 'images/default_product_image.jpg'),
(510, 'SS-E2193', '', 1000, 1650, 1, '2018-09-09 17:48:19', '2018-09-09 17:48:19', 5, 'images/default_product_image.jpg'),
(511, 'SS-E2199', '', 1000, 1650, 1, '2018-09-09 17:48:44', '2018-09-09 17:48:44', 5, 'images/default_product_image.jpg'),
(512, 'SS-H1', '', 320, 590, 1, '2018-09-09 17:49:17', '2018-09-09 17:49:17', 5, 'images/default_product_image.jpg'),
(513, 'SS-H3', '', 500, 850, 1, '2018-09-09 17:49:41', '2018-09-09 17:49:41', 5, 'images/default_product_image.jpg'),
(514, 'SS-J109-1', '', 950, 1590, 1, '2018-09-09 17:50:21', '2018-09-09 17:50:21', 5, 'images/default_product_image.jpg'),
(515, 'SS-L08', '', 1500, 2250, 1, '2018-09-09 17:51:10', '2018-09-09 17:51:10', 5, 'images/default_product_image.jpg'),
(516, 'SS-L1025', '', 1750, 2550, 1, '2018-09-09 17:51:44', '2018-09-09 17:51:44', 5, 'images/default_product_image.jpg'),
(517, 'SS-L1028', '', 1750, 2550, 1, '2018-09-09 17:52:13', '2018-09-09 17:52:13', 5, 'images/default_product_image.jpg'),
(518, 'SS-M835', '', 1000, 1680, 1, '2018-09-09 17:52:45', '2018-09-09 17:52:45', 5, 'images/default_product_image.jpg'),
(519, 'SS-NB-20', '', 950, 1590, 1, '2018-09-09 17:53:15', '2018-09-09 17:53:15', 5, 'images/default_product_image.jpg'),
(520, 'SS-Plastic', '', 350, 560, 1, '2018-09-09 17:55:29', '2018-09-09 17:55:29', 5, 'images/default_product_image.jpg'),
(521, 'SS-R36-50', '', 800, 1350, 1, '2018-09-09 17:56:10', '2018-09-09 17:56:10', 5, 'images/default_product_image.jpg'),
(522, 'SS-RW00157', '', 2450, 3590, 1, '2018-09-09 17:56:47', '2018-09-09 17:56:47', 5, 'images/default_product_image.jpg'),
(523, 'SS-T1273', '', 2150, 3250, 1, '2018-09-09 17:57:14', '2018-09-09 17:57:14', 5, 'images/default_product_image.jpg'),
(524, 'SS-T121', '', 750, 1150, 1, '2018-09-09 17:57:53', '2018-09-09 17:57:53', 5, 'images/default_product_image.jpg'),
(525, 'SS-XFA727', '', 600, 960, 1, '2018-09-09 17:58:29', '2018-09-09 17:58:29', 5, 'images/default_product_image.jpg'),
(526, 'SSE-1711', '', 500, 850, 1, '2018-09-09 17:59:09', '2018-09-09 17:59:09', 5, 'images/default_product_image.jpg'),
(527, 'SSE-18', '', 450, 780, 1, '2018-09-09 17:59:32', '2018-09-09 17:59:32', 5, 'images/default_product_image.jpg'),
(528, 'SSE-16', '', 450, 780, 1, '2018-09-09 18:00:00', '2018-09-09 18:00:00', 5, 'images/default_product_image.jpg'),
(529, 'TT-01', '', 210, 370, 1, '2018-09-09 18:05:40', '2018-09-09 18:05:40', 1, 'images/default_product_image.jpg'),
(530, 'TT-010', '', 200, 350, 1, '2018-09-09 18:06:00', '2018-09-09 18:06:00', 1, 'images/default_product_image.jpg'),
(531, 'TT-012', '', 210, 370, 1, '2018-09-09 18:06:23', '2018-09-09 18:06:23', 1, 'images/default_product_image.jpg'),
(532, 'TT-013', '', 200, 350, 1, '2018-09-09 18:06:51', '2018-09-09 18:06:51', 1, 'images/default_product_image.jpg'),
(533, 'TT-014', '', 280, 450, 1, '2018-09-09 18:07:34', '2018-09-09 18:07:34', 1, 'images/default_product_image.jpg'),
(534, 'TT-02', '', 250, 420, 1, '2018-09-09 18:08:15', '2018-09-09 18:08:15', 1, 'images/default_product_image.jpg'),
(535, 'TT-04', '', 210, 370, 1, '2018-09-09 18:08:37', '2018-09-09 18:08:37', 1, 'images/default_product_image.jpg'),
(536, 'TT-05', '', 200, 350, 1, '2018-09-09 18:09:00', '2018-09-09 18:09:00', 1, 'images/default_product_image.jpg'),
(537, 'TT-07', '', 180, 320, 1, '2018-09-09 18:09:23', '2018-09-09 18:09:23', 1, 'images/default_product_image.jpg'),
(538, 'TT-09', '', 270, 450, 1, '2018-09-09 18:09:44', '2018-09-09 18:09:44', 1, 'images/default_product_image.jpg'),
(539, 'TT-1', '', 200, 340, 1, '2018-09-09 18:10:07', '2018-09-09 18:10:07', 1, 'images/default_product_image.jpg'),
(540, 'TT-15', '', 250, 420, 1, '2018-09-09 18:10:31', '2018-09-09 18:10:31', 1, 'images/default_product_image.jpg'),
(541, 'TT-16', '', 270, 450, 1, '2018-09-09 18:10:53', '2018-09-09 18:10:53', 1, 'images/default_product_image.jpg'),
(542, 'TT-17', '', 270, 450, 1, '2018-09-09 18:11:20', '2018-09-09 18:11:20', 1, 'images/default_product_image.jpg'),
(543, 'TT-19', '', 260, 450, 1, '2018-09-09 18:11:47', '2018-09-09 18:11:47', 1, 'images/default_product_image.jpg'),
(544, 'TT-2', '', 215, 350, 1, '2018-09-09 18:12:08', '2018-09-09 18:12:08', 1, 'images/default_product_image.jpg'),
(545, 'TT-21', '', 250, 450, 1, '2018-09-09 18:12:35', '2018-09-09 18:12:35', 1, 'images/default_product_image.jpg'),
(546, 'TT-22', '', 260, 420, 1, '2018-09-09 18:12:58', '2018-09-09 18:12:58', 1, 'images/default_product_image.jpg'),
(547, 'TT-3', '', 195, 340, 1, '2018-09-09 18:13:22', '2018-09-09 18:13:22', 1, 'images/default_product_image.jpg'),
(548, 'TT-4', '', 140, 250, 1, '2018-09-09 18:13:55', '2018-09-09 18:13:55', 1, 'images/default_product_image.jpg'),
(549, 'USV-01', '', 210, 350, 1, '2018-09-09 18:35:31', '2018-09-09 18:35:31', 7, 'images/default_product_image.jpg'),
(550, 'USV-03', '', 230, 480, 1, '2018-09-09 18:35:54', '2018-09-09 18:35:54', 7, 'images/default_product_image.jpg'),
(551, 'USV-04', '', 200, 370, 1, '2018-09-09 18:36:19', '2018-09-09 18:36:19', 7, 'images/default_product_image.jpg'),
(552, 'USV-05', '', 190, 320, 1, '2018-09-09 18:36:41', '2018-09-09 18:36:41', 7, 'images/default_product_image.jpg'),
(553, 'USV-06', '', 230, 480, 1, '2018-09-09 18:37:15', '2018-09-09 18:37:15', 7, 'images/default_product_image.jpg'),
(554, 'USV-07', '', 230, 480, 1, '2018-09-09 18:37:40', '2018-09-09 18:37:40', 7, 'images/default_product_image.jpg'),
(555, 'UV-01', '', 210, 420, 1, '2018-09-09 18:38:07', '2018-09-09 18:38:07', 7, 'images/default_product_image.jpg'),
(556, 'UV-03', '', 65, 110, 1, '2018-09-09 18:38:34', '2018-09-09 18:38:34', 7, 'images/default_product_image.jpg'),
(557, 'UV-05', '', 320, 560, 1, '2018-09-09 18:38:58', '2018-09-09 18:38:58', 7, 'images/default_product_image.jpg'),
(558, 'UV-06', '', 300, 520, 1, '2018-09-09 18:39:25', '2018-09-09 18:39:25', 7, 'images/default_product_image.jpg'),
(559, 'UV-07', '', 250, 590, 1, '2018-09-09 18:40:08', '2018-09-09 18:40:08', 7, 'images/default_product_image.jpg'),
(560, 'UV-08', '', 230, 520, 1, '2018-09-09 18:40:39', '2018-09-09 18:40:39', 7, 'images/default_product_image.jpg'),
(561, 'UV-09', '', 280, 520, 1, '2018-09-09 18:41:35', '2018-09-09 18:41:35', 7, 'images/default_product_image.jpg'),
(562, 'UV-12', '', 700, 1120, 1, '2018-09-09 18:42:04', '2018-09-09 18:42:04', 7, 'images/default_product_image.jpg'),
(563, 'VN-11', '', 110, 220, 1, '2018-09-09 18:43:12', '2018-09-09 18:43:12', 9, 'images/default_product_image.jpg'),
(564, 'VN-15', '', 75, 190, 1, '2018-09-09 18:43:59', '2018-09-09 18:43:59', 9, 'images/default_product_image.jpg'),
(565, 'VN-19', '', 110, 220, 1, '2018-09-09 18:44:39', '2018-09-09 18:44:39', 9, 'images/default_product_image.jpg'),
(566, 'VN-2', '', 90, 150, 1, '2018-09-09 18:45:00', '2018-09-09 18:45:00', 9, 'images/default_product_image.jpg'),
(567, 'VN-20', '', 90, 450, 1, '2018-09-09 18:45:29', '2018-09-09 18:45:29', 9, 'images/default_product_image.jpg'),
(568, 'VN-22', '', 115, 250, 1, '2018-09-09 18:46:12', '2018-09-09 18:46:12', 9, 'images/default_product_image.jpg'),
(569, 'VN-26', '', 1400, 1850, 1, '2018-09-09 18:46:37', '2018-09-09 18:46:37', 9, 'images/default_product_image.jpg'),
(570, 'VN-27', '', 1100, 1790, 1, '2018-09-09 18:47:13', '2018-09-09 18:47:13', 9, 'images/default_product_image.jpg'),
(571, 'VN-3', '', 120, 220, 1, '2018-09-09 18:47:38', '2018-09-09 18:47:38', 9, 'images/default_product_image.jpg'),
(572, 'VN-5', '', 220, 550, 1, '2018-09-09 18:48:03', '2018-09-09 18:48:03', 9, 'images/default_product_image.jpg'),
(573, 'VN-6', '', 1050, 1650, 1, '2018-09-09 18:48:24', '2018-09-09 18:48:24', 9, 'images/default_product_image.jpg'),
(574, 'VN-04', '', 190, 450, 1, '2018-09-09 18:48:55', '2018-09-09 18:48:55', 9, 'images/default_product_image.jpg'),
(575, 'WCS-180', '', 1025, 1680, 1, '2018-09-09 18:49:24', '2018-09-09 18:49:24', 5, 'images/default_product_image.jpg'),
(576, 'WCS-6113', '', 900, 1570, 1, '2018-09-09 18:49:52', '2018-09-09 18:49:52', 5, 'images/default_product_image.jpg'),
(577, 'WCS-7-17', '', 780, 1250, 1, '2018-09-09 18:50:37', '2018-09-09 18:50:37', 5, 'images/default_product_image.jpg'),
(578, 'WCS-B801', '', 690, 1150, 1, '2018-09-09 18:51:12', '2018-09-09 18:51:12', 5, 'images/default_product_image.jpg'),
(579, 'YC-2188-3', '', 750, 1150, 1, '2018-09-09 18:51:48', '2018-09-09 18:51:48', 5, 'images/default_product_image.jpg'),
(580, 'YC-HF25-151', '', 800, 1350, 1, '2018-09-09 18:52:29', '2018-09-09 18:52:29', 5, 'images/default_product_image.jpg'),
(581, 'YC-HF27-17', '', 800, 1350, 1, '2018-09-09 18:53:07', '2018-09-09 18:53:07', 5, 'images/default_product_image.jpg'),
(582, 'YC-TD60', '', 1100, 1850, 1, '2018-09-09 18:53:54', '2018-09-09 18:53:54', 5, 'images/default_product_image.jpg'),
(583, 'ZKT-13', '', 180, 290, 1, '2018-09-09 18:54:32', '2018-09-09 18:54:32', 11, 'images/default_product_image.jpg'),
(584, 'ZT-Moon15', '', 240, 400, 1, '2018-09-09 18:55:14', '2018-09-09 18:55:14', 2, 'images/default_product_image.jpg'),
(585, 'ZT-R12', '', 240, 390, 1, '2018-09-09 19:01:44', '2018-09-09 19:01:44', 2, 'images/default_product_image.jpg'),
(586, 'ZT-S10', '', 235, 380, 1, '2018-09-09 19:02:18', '2018-09-09 19:02:18', 2, 'images/default_product_image.jpg'),
(587, 'ZT-S6', '', 225, 380, 1, '2018-09-09 19:02:48', '2018-09-09 19:02:48', 2, 'images/default_product_image.jpg'),
(588, 'ZT-U13', '', 103, 140, 1, '2018-09-09 19:03:27', '2018-09-09 19:03:27', 2, 'images/default_product_image.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `product_variant`
--

CREATE TABLE `product_variant` (
  `product_variant_id` int(11) NOT NULL,
  `size` varchar(45) NOT NULL,
  `color` varchar(45) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_variant`
--

INSERT INTO `product_variant` (`product_variant_id`, `size`, `color`, `quantity`, `product_id`) VALUES
(1, 'None', 'None', 1, 1),
(2, 'None', 'None', 1, 2),
(3, 'None', 'None', 5, 3),
(4, 'None', 'None', 20, 4),
(5, 'None', 'None', 3, 5),
(6, 'None', 'None', 6, 6),
(7, 'None', 'None', 9, 7),
(8, 'None', 'None', 1, 8),
(9, 'None', 'None', 2, 9),
(10, 'None', 'None', 4, 10),
(11, 'None', 'None', 22, 11),
(12, 'None', 'None', 20, 12),
(13, 'None', 'None', 3, 13),
(14, 'None', 'None', 4, 14),
(15, 'None', 'None', 4, 15),
(16, 'None', 'None', 1, 16),
(17, 'None', 'None', 3, 17),
(18, 'None', 'None', 2, 18),
(19, 'None', 'None', 2, 19),
(20, 'None', 'None', 2, 21),
(21, 'None', 'None', 2, 22),
(22, 'None', 'None', 1, 23),
(23, 'None', 'None', 24, 24),
(24, 'None', 'None', 6, 25),
(25, 'None', 'None', 7, 26),
(26, 'None', 'None', 2, 27),
(27, 'None', 'None', 0, 28),
(28, 'None', 'None', 1, 29),
(29, 'None', 'None', 2, 30),
(30, 'None', 'None', 1, 31),
(31, 'None', 'None', 6, 32),
(32, 'None', 'None', 2, 33),
(33, 'None', 'None', 4, 34),
(34, 'None', 'None', 1, 35),
(35, 'None', 'None', 0, 36),
(36, 'None', 'None', 14, 37),
(37, 'None', 'None', 3, 38),
(38, 'None', 'None', 2, 39),
(39, 'None', 'None', 9, 40),
(40, 'None', 'None', 16, 41),
(41, 'None', 'None', 16, 42),
(42, 'None', 'None', 3, 43),
(43, 'None', 'None', 1, 44),
(44, 'None', 'None', 3, 45),
(45, 'None', 'None', 4, 46),
(46, 'None', 'None', 1, 47),
(47, 'None', 'None', 5, 48),
(48, 'None', 'None', 3, 49),
(49, 'None', 'None', 2, 50),
(50, 'None', 'None', 8, 51),
(51, 'None', 'None', 1, 52),
(52, 'None', 'None', 5, 53),
(53, 'None', 'None', 1, 54),
(54, 'None', 'None', 1, 55),
(55, 'None', 'None', 1, 56),
(56, 'None', 'None', 2, 57),
(57, 'None', 'None', 1, 58),
(58, 'None', 'None', 3, 59),
(59, 'None', 'None', 2, 60),
(60, 'None', 'None', 1, 61),
(61, 'None', 'None', 2, 62),
(62, 'None', 'None', 2, 63),
(63, 'None', 'None', 1, 64),
(64, 'None', 'None', 1, 65),
(65, 'None', 'None', 1, 66),
(66, 'None', 'None', 1, 67),
(67, 'None', 'None', 2, 68),
(68, 'None', 'None', 1, 69),
(69, 'None', 'None', 1, 70),
(70, 'None', 'None', 2, 71),
(71, 'None', 'None', 4, 72),
(72, 'None', 'None', 2, 73),
(73, 'None', 'None', 3, 74),
(74, 'None', 'None', 2, 75),
(75, 'None', 'None', 2, 76),
(76, 'None', 'None', 2, 77),
(77, 'None', 'None', 5, 78),
(78, 'None', 'None', 3, 79),
(79, 'None', 'None', 4, 80),
(80, 'None', 'None', 5, 81),
(81, 'None', 'None', 2, 82),
(82, 'None', 'None', 12, 83),
(83, 'None', 'None', 1, 84),
(84, 'None', 'None', 3, 85),
(85, 'None', 'None', 3, 86),
(86, 'None', 'None', 5, 87),
(87, 'None', 'None', 3, 88),
(88, 'None', 'None', 7, 89),
(89, 'None', 'None', 5, 90),
(90, 'None', 'None', 8, 91),
(91, 'None', 'None', 9, 92),
(92, 'None', 'None', 1, 93),
(93, 'None', 'None', 18, 94),
(94, 'None', 'None', 4, 95),
(95, 'None', 'None', 3, 96),
(96, 'None', 'None', 2, 97),
(97, 'None', 'None', 4, 98),
(98, 'None', 'None', 4, 99),
(99, 'None', 'None', 1, 100),
(100, 'None', 'None', 12, 101),
(101, 'None', 'None', 2, 102),
(102, 'None', 'None', 5, 103),
(103, 'None', 'None', 5, 104),
(104, 'None', 'None', 2, 105),
(105, 'None', 'None', 1, 106),
(106, 'None', 'None', 5, 107),
(107, 'None', 'None', 1, 108),
(108, 'None', 'None', 1, 109),
(109, 'None', 'None', 2, 110),
(110, 'None', 'None', 8, 111),
(111, 'None', 'None', 1, 112),
(112, 'None', 'None', 23, 113),
(113, 'None', 'None', 8, 114),
(114, 'None', 'None', 4, 115),
(115, 'None', 'None', 4, 116),
(116, 'None', 'None', 4, 117),
(117, 'None', 'None', 3, 118),
(118, 'None', 'None', 0, 119),
(119, 'None', 'None', 6, 120),
(120, 'None', 'None', 5, 121),
(121, 'None', 'None', 0, 122),
(122, 'None', 'None', 3, 123),
(123, 'None', 'None', 6, 124),
(124, 'None', 'None', 7, 125),
(125, 'None', 'None', 5, 126),
(126, 'None', 'None', 4, 127),
(127, 'None', 'None', 3, 128),
(128, 'None', 'None', 44, 129),
(129, 'None', 'None', 2, 130),
(130, 'None', 'None', 2, 131),
(131, 'None', 'None', 2, 132),
(132, 'None', 'None', 2, 133),
(133, 'None', 'None', 2, 134),
(134, 'None', 'None', 1, 135),
(135, 'None', 'None', 4, 136),
(136, 'None', 'None', 8, 137),
(137, 'None', 'None', 4, 138),
(138, 'None', 'None', 3, 139),
(139, 'None', 'None', 10, 140),
(140, 'None', 'None', 2, 141),
(141, 'None', 'None', 5, 142),
(142, 'None', 'None', 5, 143),
(143, 'None', 'None', 6, 144),
(144, 'None', 'None', 25, 145),
(145, 'None', 'None', 2, 146),
(146, 'None', 'None', 4, 147),
(147, 'None', 'None', 2, 148),
(148, 'None', 'None', 5, 149),
(149, 'None', 'None', 8, 150),
(150, 'None', 'None', 9, 151),
(151, 'None', 'None', 4, 152),
(152, 'None', 'None', 6, 153),
(153, 'None', 'None', 1, 154),
(154, 'None', 'None', 5, 155),
(155, 'None', 'None', 2, 156),
(156, 'None', 'None', 2, 158),
(157, 'None', 'None', 5, 159),
(158, 'None', 'None', 3, 160),
(159, 'None', 'None', 4, 161),
(160, 'None', 'None', 12, 162),
(161, 'None', 'None', 5, 163),
(162, 'None', 'None', 11, 164),
(163, 'None', 'None', 3, 165),
(164, 'None', 'None', 3, 166),
(165, 'None', 'None', 5, 167),
(166, 'None', 'None', 3, 168),
(167, 'None', 'None', 4, 169),
(168, 'None', 'None', 2, 170),
(169, 'None', 'None', 3, 171),
(170, 'None', 'None', 4, 172),
(171, 'None', 'None', 1, 173),
(172, 'None', 'None', 3, 174),
(173, 'None', 'None', 1, 175),
(174, 'None', 'None', 7, 176),
(175, 'None', 'None', 3, 177),
(176, 'None', 'None', 4, 178),
(177, 'None', 'None', 8, 179),
(178, 'None', 'None', 12, 180),
(179, 'None', 'None', 6, 181),
(180, 'None', 'None', 9, 182),
(181, 'None', 'None', 9, 183),
(182, 'None', 'None', 7, 184),
(183, 'None', 'None', 4, 185),
(184, 'None', 'None', 8, 186),
(185, 'None', 'None', 11, 187),
(186, 'None', 'None', 5, 188),
(187, 'None', 'None', 2, 189),
(188, 'None', 'None', 1, 190),
(189, 'None', 'None', 3, 191),
(190, 'None', 'None', 2, 192),
(191, 'None', 'None', 1, 193),
(192, 'None', 'None', 5, 194),
(193, 'None', 'None', 17, 195),
(194, 'None', 'None', 7, 196),
(195, 'None', 'None', 12, 197),
(196, 'None', 'None', 10, 198),
(197, 'None', 'None', 4, 199),
(198, 'None', 'None', 2, 200),
(199, 'None', 'None', 5, 201),
(200, 'None', 'None', 2, 202),
(201, 'None', 'None', 2, 203),
(202, 'None', 'None', 2, 204),
(203, 'None', 'None', 11, 205),
(204, 'None', 'None', 2, 206),
(205, 'None', 'None', 3, 207),
(206, 'None', 'None', 4, 208),
(207, 'None', 'None', 4, 209),
(208, 'None', 'None', 2, 210),
(209, 'None', 'None', 4, 211),
(210, 'None', 'None', 3, 212),
(211, 'None', 'None', 2, 213),
(212, 'None', 'None', 1, 214),
(213, 'None', 'None', 2, 215),
(214, 'None', 'None', 1, 216),
(215, 'None', 'None', 2, 217),
(216, 'None', 'None', 1, 218),
(217, 'None', 'None', 0, 219),
(218, 'None', 'None', 4, 220),
(219, 'None', 'None', 6, 221),
(220, 'None', 'None', 0, 222),
(221, 'None', 'None', 1, 223),
(222, 'None', 'None', 3, 224),
(223, 'None', 'None', 4, 225),
(224, 'None', 'None', 4, 226),
(225, 'None', 'None', 4, 227),
(226, 'None', 'None', 3, 228),
(227, 'None', 'None', 2, 229),
(228, 'None', 'None', 3, 230),
(229, 'None', 'None', 3, 231),
(230, 'None', 'None', 5, 232),
(231, 'None', 'None', 12, 233),
(232, 'None', 'None', 5, 234),
(233, 'None', 'None', 1, 235),
(234, 'None', 'None', 1, 236),
(235, 'None', 'None', 1, 237),
(236, 'None', 'None', 1, 238),
(237, 'None', 'None', 94, 239),
(238, 'None', 'None', 3, 240),
(239, 'None', 'None', 0, 241),
(240, 'None', 'None', 7, 242),
(241, 'None', 'None', 37, 243),
(242, 'None', 'None', 2, 244),
(243, 'None', 'None', 10, 245),
(244, 'None', 'None', 1, 246),
(245, 'None', 'None', 2, 247),
(246, 'None', 'None', 5, 248),
(247, 'None', 'None', 44, 249),
(248, 'None', 'None', 2, 250),
(249, 'None', 'None', 1, 251),
(250, 'None', 'None', 4, 252),
(251, 'None', 'None', 3, 253),
(252, 'None', 'None', 8, 254),
(253, 'None', 'None', 5, 255),
(254, 'None', 'None', 5, 256),
(255, 'None', 'None', 2, 257),
(256, 'None', 'None', 4, 258),
(257, 'None', 'None', 3, 259),
(258, 'None', 'None', 1, 260),
(259, 'None', 'None', 1, 261),
(260, 'None', 'None', 1, 262),
(261, 'None', 'None', 1, 263),
(262, 'None', 'None', 1, 264),
(263, 'None', 'None', 27, 265),
(264, 'None', 'None', 64, 266),
(265, 'None', 'None', 39, 267),
(266, 'None', 'None', 33, 268),
(267, 'None', 'None', 34, 269),
(268, 'None', 'None', 142, 270),
(269, 'None', 'None', 37, 271),
(270, 'None', 'None', 18, 272),
(271, 'None', 'None', 4, 273),
(272, 'None', 'None', 13, 274),
(273, 'None', 'None', 4, 275),
(274, 'None', 'None', 12, 276),
(275, 'None', 'None', 8, 277),
(276, 'None', 'None', 7, 278),
(277, 'None', 'None', 2, 279),
(278, 'None', 'None', 3, 280),
(279, 'None', 'None', 7, 281),
(280, 'None', 'None', 6, 282),
(281, 'None', 'None', 20, 283),
(282, 'None', 'None', 5, 284),
(283, 'None', 'None', 5, 285),
(284, 'None', 'None', 8, 286),
(285, 'None', 'None', 5, 287),
(286, 'None', 'None', 1, 288),
(287, 'None', 'None', 9, 289),
(288, 'None', 'None', 3, 290),
(289, 'None', 'None', 5, 291),
(290, 'None', 'None', 7, 292),
(291, 'None', 'None', 6, 293),
(292, 'None', 'None', 2, 294),
(293, 'None', 'None', 4, 295),
(294, 'None', 'None', 4, 296),
(295, 'None', 'None', 2, 297),
(296, 'None', 'None', 17, 298),
(297, 'None', 'None', 4, 299),
(298, 'None', 'None', 7, 300),
(299, 'None', 'None', 7, 301),
(300, 'None', 'None', 3, 302),
(301, 'None', 'None', 148, 303),
(302, 'None', 'None', 2, 304),
(303, 'None', 'None', 1, 305),
(304, 'None', 'None', 3, 306),
(305, 'None', 'None', 2, 307),
(306, 'None', 'None', 2, 308),
(307, 'None', 'None', 1, 309),
(308, 'None', 'None', 7, 310),
(309, 'None', 'None', 8, 311),
(310, 'None', 'None', 61, 312),
(311, 'None', 'None', 2, 313),
(312, 'None', 'None', 20, 314),
(313, 'None', 'None', 16, 315),
(314, 'None', 'None', 29, 316),
(315, 'None', 'None', 26, 317),
(316, 'None', 'None', 3, 318),
(317, 'None', 'None', 3, 319),
(318, 'None', 'None', 3, 320),
(319, 'None', 'None', 12, 321),
(320, 'None', 'None', 1, 322),
(321, 'None', 'None', 5, 323),
(322, 'None', 'None', 1, 324),
(323, 'None', 'None', 2, 325),
(324, 'None', 'None', 3, 326),
(325, 'None', 'None', 19, 327),
(326, 'None', 'None', 4, 328),
(327, 'None', 'None', 1, 329),
(328, 'None', 'None', 1, 330),
(329, 'None', 'None', 1, 331),
(330, 'None', 'None', 5, 332),
(331, 'None', 'None', 4, 333),
(332, 'None', 'None', 3, 334),
(333, 'None', 'None', 2, 335),
(334, 'None', 'None', 1, 336),
(335, 'None', 'None', 2, 337),
(336, 'None', 'None', 2, 338),
(337, 'None', 'None', 1, 339),
(338, 'None', 'None', 1, 340),
(339, 'None', 'None', 11, 341),
(340, 'None', 'None', 1, 342),
(341, 'None', 'None', 2, 343),
(342, 'None', 'None', 1, 344),
(343, 'None', 'None', 1, 345),
(344, 'None', 'None', 2, 346),
(345, 'None', 'None', 2, 347),
(346, 'None', 'None', 2, 348),
(347, 'None', 'None', 1, 349),
(348, 'None', 'None', 2, 350),
(349, 'None', 'None', 1, 351),
(350, 'None', 'None', 1, 352),
(351, 'None', 'None', 2, 353),
(352, 'None', 'None', 14, 354),
(353, 'None', 'None', 1, 355),
(354, 'None', 'None', 1, 356),
(355, 'None', 'None', 1, 357),
(356, 'None', 'None', 3, 358),
(357, 'None', 'None', 1, 359),
(358, 'None', 'None', 1, 360),
(359, 'None', 'None', 1, 361),
(360, 'None', 'None', 1, 362),
(361, 'None', 'None', 1, 363),
(362, 'None', 'None', 2, 364),
(363, 'None', 'None', 1, 365),
(364, 'None', 'None', 3, 366),
(365, 'None', 'None', 1, 367),
(366, 'None', 'None', 1, 368),
(367, 'None', 'None', 2, 369),
(368, 'None', 'None', 2, 370),
(369, 'None', 'None', 1, 371),
(370, 'None', 'None', 1, 372),
(371, 'None', 'None', 2, 373),
(372, 'None', 'None', 4, 374),
(373, 'None', 'None', 3, 375),
(374, 'None', 'None', 1, 376),
(375, 'None', 'None', 11, 377),
(376, 'None', 'None', 6, 378),
(377, 'None', 'None', 8, 379),
(378, 'None', 'None', 5, 380),
(379, 'None', 'None', 5, 381),
(380, 'None', 'None', 1, 382),
(381, 'None', 'None', 5, 383),
(382, 'None', 'None', 5, 384),
(383, 'None', 'None', 10, 385),
(384, 'None', 'None', 2, 386),
(385, 'None', 'None', 1, 387),
(386, 'None', 'None', 6, 388),
(387, 'None', 'None', 6, 389),
(388, 'None', 'None', 2, 390),
(389, 'None', 'None', 0, 391),
(390, 'None', 'None', 4, 392),
(391, 'None', 'None', 6, 393),
(392, 'None', 'None', 4, 394),
(393, 'None', 'None', 2, 395),
(394, 'None', 'None', 5, 396),
(395, 'None', 'None', 3, 397),
(396, 'None', 'None', 9, 398),
(397, 'None', 'None', 3, 399),
(398, 'None', 'None', 4, 400),
(399, 'None', 'None', 3, 401),
(400, 'None', 'None', 2, 402),
(401, 'None', 'None', 4, 403),
(402, 'None', 'None', 2, 404),
(403, 'None', 'None', 2, 405),
(404, 'None', 'None', 17, 406),
(405, 'None', 'None', 6, 407),
(406, 'None', 'None', 12, 408),
(407, 'None', 'None', 6, 409),
(408, 'None', 'None', 5, 410),
(409, 'None', 'None', 5, 411),
(410, 'None', 'None', 4, 412),
(411, 'None', 'None', 10, 413),
(412, 'None', 'None', 1, 414),
(413, 'None', 'None', 2, 415),
(414, 'None', 'None', 6, 416),
(415, 'None', 'None', 10, 417),
(416, 'None', 'None', 5, 418),
(417, 'None', 'None', 5, 419),
(418, 'None', 'None', 8, 420),
(419, 'None', 'None', 10, 421),
(420, 'None', 'None', 11, 422),
(421, 'None', 'None', 6, 423),
(422, 'None', 'None', 10, 424),
(423, 'None', 'None', 6, 425),
(424, 'None', 'None', 10, 426),
(425, 'None', 'None', 5, 427),
(426, 'None', 'None', 6, 428),
(427, 'None', 'None', 6, 429),
(428, 'None', 'None', 8, 430),
(429, 'None', 'None', 5, 431),
(430, 'None', 'None', 3, 432),
(431, 'None', 'None', 4, 433),
(432, 'None', 'None', 2, 434),
(433, 'None', 'None', 4, 435),
(434, 'None', 'None', 1, 436),
(435, 'None', 'None', 1, 437),
(436, 'None', 'None', 5, 438),
(437, 'None', 'None', 9, 439),
(438, 'None', 'None', 3, 440),
(439, 'None', 'None', 1, 441),
(440, 'None', 'None', 4, 442),
(441, 'None', 'None', 1, 443),
(442, 'None', 'None', 5, 444),
(443, 'None', 'None', 3, 445),
(444, 'None', 'None', 1, 446),
(445, 'None', 'None', 2, 447),
(446, 'None', 'None', 2, 448),
(447, 'None', 'None', 4, 449),
(448, 'None', 'None', 4, 450),
(449, 'None', 'None', 4, 451),
(450, 'None', 'None', 5, 452),
(451, 'None', 'None', 2, 453),
(452, 'None', 'None', 2, 454),
(453, 'None', 'None', 6, 455),
(454, 'None', 'None', 7, 456),
(455, 'None', 'None', 7, 457),
(456, 'None', 'None', 10, 458),
(457, 'None', 'None', 6, 459),
(458, 'None', 'None', 3, 460),
(459, 'None', 'None', 6, 461),
(460, 'None', 'None', 3, 462),
(461, 'None', 'None', 6, 463),
(462, 'None', 'None', 12, 464),
(463, 'None', 'None', 3, 465),
(464, 'None', 'None', 8, 466),
(465, 'None', 'None', 6, 467),
(466, 'None', 'None', 10, 468),
(467, 'None', 'None', 6, 469),
(468, 'None', 'None', 5, 470),
(469, 'None', 'None', 1, 471),
(470, 'None', 'None', 3, 472),
(471, 'None', 'None', 10, 473),
(472, 'None', 'None', 2, 474),
(473, 'None', 'None', 3, 475),
(474, 'None', 'None', 3, 476),
(475, 'None', 'None', 1, 477),
(476, 'None', 'None', 3, 478),
(477, 'None', 'None', 5, 479),
(478, 'None', 'None', 4, 480),
(479, 'None', 'None', 8, 481),
(480, 'None', 'None', 1, 482),
(481, 'None', 'None', 5, 483),
(482, 'None', 'None', 1, 484),
(483, 'None', 'None', 1, 485),
(484, 'None', 'None', 14, 486),
(485, 'None', 'None', 3, 487),
(486, 'None', 'None', 2, 488),
(487, 'None', 'None', 4, 489),
(488, 'None', 'None', 20, 490),
(489, 'None', 'None', 1, 491),
(490, 'None', 'None', 6, 492),
(491, 'None', 'None', 4, 493),
(492, 'None', 'None', 6, 494),
(493, 'None', 'None', 1, 495),
(494, 'None', 'None', 9, 496),
(495, 'None', 'None', 4, 497),
(496, 'None', 'None', 3, 498),
(497, 'None', 'None', 1, 499),
(498, 'None', 'None', 1, 500),
(499, 'None', 'None', 1, 501),
(500, 'None', 'None', 4, 502),
(501, 'None', 'None', 6, 503),
(502, 'None', 'None', 6, 504),
(503, 'None', 'None', 5, 505),
(504, 'None', 'None', 3, 506),
(505, 'None', 'None', 6, 507),
(506, 'None', 'None', 4, 508),
(507, 'None', 'None', 4, 509),
(508, 'None', 'None', 3, 510),
(509, 'None', 'None', 3, 511),
(510, 'None', 'None', 1, 512),
(511, 'None', 'None', 3, 513),
(512, 'None', 'None', 3, 514),
(513, 'None', 'None', 3, 515),
(514, 'None', 'None', 1, 516),
(515, 'None', 'None', 1, 517),
(516, 'None', 'None', 5, 518),
(517, 'None', 'None', 2, 519),
(518, 'None', 'None', 56, 520),
(519, 'None', 'None', 1, 521),
(520, 'None', 'None', 2, 522),
(521, 'None', 'None', 3, 523),
(522, 'None', 'None', 3, 524),
(523, 'None', 'None', 5, 525),
(524, 'None', 'None', 8, 526),
(525, 'None', 'None', 2, 527),
(526, 'None', 'None', 4, 528),
(527, 'None', 'None', 12, 529),
(528, 'None', 'None', 5, 530),
(529, 'None', 'None', 3, 531),
(530, 'None', 'None', 2, 532),
(531, 'None', 'None', 2, 533),
(532, 'None', 'None', 2, 534),
(533, 'None', 'None', 5, 535),
(534, 'None', 'None', 10, 536),
(535, 'None', 'None', 6, 537),
(536, 'None', 'None', 9, 538),
(537, 'None', 'None', 1, 539),
(538, 'None', 'None', 3, 540),
(539, 'None', 'None', 6, 541),
(540, 'None', 'None', 4, 542),
(541, 'None', 'None', 2, 543),
(542, 'None', 'None', 3, 544),
(543, 'None', 'None', 2, 545),
(544, 'None', 'None', 1, 546),
(545, 'None', 'None', 8, 547),
(546, 'None', 'None', 9, 548),
(547, 'None', 'None', 17, 549),
(548, 'None', 'None', 14, 550),
(549, 'None', 'None', 7, 551),
(550, 'None', 'None', 10, 552),
(551, 'None', 'None', 20, 553),
(552, 'None', 'None', 24, 554),
(553, 'None', 'None', 32, 555),
(554, 'None', 'None', 20, 556),
(555, 'None', 'None', 3, 557),
(556, 'None', 'None', 2, 558),
(557, 'None', 'None', 1, 559),
(558, 'None', 'None', 1, 560),
(559, 'None', 'None', 8, 561),
(560, 'None', 'None', 2, 562),
(561, 'None', 'None', 3, 563),
(562, 'None', 'None', 2, 564),
(563, 'None', 'None', 2, 565),
(564, 'None', 'None', 2, 566),
(565, 'None', 'None', 6, 567),
(566, 'None', 'None', 4, 568),
(567, 'None', 'None', 1, 569),
(568, 'None', 'None', 2, 570),
(569, 'None', 'None', 4, 571),
(570, 'None', 'None', 4, 572),
(571, 'None', 'None', 1, 573),
(572, 'None', 'None', 3, 574),
(573, 'None', 'None', 4, 575),
(574, 'None', 'None', 4, 576),
(575, 'None', 'None', 2, 577),
(576, 'None', 'None', 9, 578),
(577, 'None', 'None', 4, 579),
(578, 'None', 'None', 1, 580),
(579, 'None', 'None', 2, 581),
(580, 'None', 'None', 4, 582),
(581, 'None', 'None', 3, 583),
(582, 'None', 'None', 6, 584),
(583, 'None', 'None', 6, 585),
(584, 'None', 'None', 1, 586),
(585, 'None', 'None', 1, 587),
(586, 'None', 'None', 7, 588);

-- --------------------------------------------------------

--
-- Table structure for table `returned_order`
--

CREATE TABLE `returned_order` (
  `returned_order_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `returned_date` datetime NOT NULL,
  `total_returned_amount` float NOT NULL,
  `total_returned_charge` float NOT NULL,
  `remark` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `returned_order`
--

INSERT INTO `returned_order` (`returned_order_id`, `order_id`, `returned_date`, `total_returned_amount`, `total_returned_charge`, `remark`) VALUES
(1, 10, '2018-09-09 22:02:33', 110, 0, ''),
(2, 11, '2018-09-09 22:02:49', 110, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `returned_product`
--

CREATE TABLE `returned_product` (
  `product_id` int(11) NOT NULL,
  `returned_quantity` int(11) NOT NULL,
  `returned_amount` float NOT NULL,
  `returned_charge` float NOT NULL,
  `returned_order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `returned_product`
--

INSERT INTO `returned_product` (`product_id`, `returned_quantity`, `returned_amount`, `returned_charge`, `returned_order_id`) VALUES
(198, 1, 0, 0, 1),
(198, 1, 0, 0, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sell_log`
--

CREATE TABLE `sell_log` (
  `id` int(11) NOT NULL,
  `user_first_name` varchar(255) NOT NULL,
  `user_last_name` varchar(255) NOT NULL,
  `user_phone` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `user_group` varchar(60) NOT NULL,
  `discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sell_log`
--

INSERT INTO `sell_log` (`id`, `user_first_name`, `user_last_name`, `user_phone`, `date`, `user_group`, `discount`) VALUES
(1, 'admin', 'nobabee', '', '2018-09-08 18:11:45', '1', 500),
(2, 'admin', 'nobabee', '', '2018-09-08 19:00:08', '1', 50),
(3, 'admin', 'nobabee', '', '2018-09-09 17:01:21', '1', 100),
(4, 'admin', 'nobabee', '', '2018-09-09 17:10:58', '1', 250),
(5, 'admin', 'nobabee', '', '2018-09-09 17:44:30', '1', 90),
(6, 'admin', 'nobabee', '', '2018-09-09 18:00:57', '1', 10),
(7, 'admin', 'nobabee', '', '2018-09-09 19:10:59', '1', 180),
(8, 'admin', 'nobabee', '', '2018-09-09 21:03:11', '1', 50),
(9, 'admin', 'nobabee', '', '2018-09-09 21:48:09', '1', 50),
(10, 'admin', 'nobabee', '', '2018-09-09 21:54:04', '1', 10),
(11, 'admin', 'nobabee', '', '2018-09-09 21:56:13', '1', 10),
(12, 'admin', 'nobabee', '', '2018-09-09 22:02:33', '1', 0),
(13, 'admin', 'nobabee', '', '2018-09-09 22:02:49', '1', 0),
(14, 'admin', 'nobabee', '', '2018-09-10 15:17:43', '1', 100),
(15, 'admin', 'nobabee', '', '2018-09-10 15:19:24', '1', 100),
(16, 'admin', 'nobabee', '', '2018-09-10 19:50:07', '1', 250),
(17, 'admin', 'nobabee', '', '2018-09-10 21:09:18', '1', 50),
(18, 'admin', 'nobabee', '', '2018-09-11 10:54:29', '1', 390),
(19, 'admin', 'nobabee', '', '2018-09-11 11:40:38', '1', 100),
(20, 'admin', 'nobabee', '', '2018-09-11 11:53:39', '1', 110),
(21, 'admin', 'nobabee', '', '2018-09-11 16:31:47', '1', 50),
(22, 'admin', 'nobabee', '', '2018-09-11 18:39:50', '1', 130),
(23, 'admin', 'nobabee', '', '2018-09-11 19:22:42', '1', 150),
(24, 'admin', 'nobabee', '', '2018-09-12 10:26:55', '1', 70),
(25, 'admin', 'nobabee', '', '2018-09-12 11:42:49', '1', 10),
(26, 'admin', 'nobabee', '', '2018-09-12 17:44:04', '1', 10),
(27, 'admin', 'nobabee', '', '2018-09-12 17:54:11', '1', 50),
(28, 'admin', 'nobabee', '', '2018-09-12 18:34:53', '1', 100),
(29, 'admin', 'nobabee', '', '2018-09-12 18:50:10', '1', 30),
(30, 'admin', 'nobabee', '', '2018-09-12 19:57:57', '1', 50),
(31, 'admin', 'nobabee', '', '2018-09-12 20:03:36', '1', 190),
(32, 'admin', 'nobabee', '', '2018-09-12 20:44:31', '1', 30),
(33, 'admin', 'nobabee', '', '2018-09-12 21:26:20', '1', 100),
(34, 'admin', 'nobabee', '', '2018-09-13 18:30:13', '1', 0),
(35, 'admin', 'nobabee', '', '2018-09-13 19:27:16', '1', 80),
(36, 'admin', 'nobabee', '', '2018-09-13 19:35:24', '1', 90),
(37, 'admin', 'nobabee', '', '2018-09-13 19:36:55', '1', 5),
(38, 'admin', 'nobabee', '', '2018-09-13 19:40:44', '1', 20),
(39, 'admin', 'nobabee', '', '2018-09-13 19:44:16', '1', 70),
(40, 'admin', 'nobabee', '', '2018-09-13 19:47:44', '1', 2),
(41, 'admin', 'nobabee', '', '2018-09-13 20:30:02', '1', 50),
(42, 'admin', 'nobabee', '', '2018-09-13 20:32:18', '1', 10),
(43, 'admin', 'nobabee', '', '2018-09-13 20:49:29', '1', 200),
(44, 'admin', 'nobabee', '', '2018-09-14 18:14:45', '1', 10),
(45, 'admin', 'nobabee', '', '2018-09-14 18:33:59', '1', 10),
(46, 'admin', 'nobabee', '', '2018-09-14 18:37:03', '1', 90),
(47, 'admin', 'nobabee', '', '2018-09-14 18:41:42', '1', 70),
(48, 'admin', 'nobabee', '', '2018-09-14 18:45:54', '1', 50),
(49, 'admin', 'nobabee', '', '2018-09-14 18:49:12', '1', 10),
(50, 'admin', 'nobabee', '', '2018-09-14 19:38:40', '1', 50),
(51, 'admin', 'nobabee', '', '2018-09-14 19:53:07', '1', 50),
(52, 'admin', 'nobabee', '', '2018-09-14 20:04:36', '1', 120),
(53, 'admin', 'nobabee', '', '2018-09-14 20:23:31', '1', 20),
(54, 'admin', 'nobabee', '', '2018-09-14 20:26:02', '1', 190),
(55, 'admin', 'nobabee', '', '2018-09-14 20:33:56', '1', 30),
(56, 'admin', 'nobabee', '', '2018-09-15 11:52:56', '1', 140),
(57, 'admin', 'nobabee', '', '2018-09-15 14:54:51', '1', 10),
(58, 'admin', 'nobabee', '', '2018-09-15 16:20:20', '1', 10),
(59, 'admin', 'nobabee', '', '2018-09-15 18:47:14', '1', 180),
(60, 'admin', 'nobabee', '', '2018-09-15 19:14:20', '1', 40),
(61, 'admin', 'nobabee', '', '2018-09-15 19:53:38', '1', 50),
(62, 'admin', 'nobabee', '', '2018-09-15 21:06:11', '1', 40);

-- --------------------------------------------------------

--
-- Table structure for table `sell_log_detail`
--

CREATE TABLE `sell_log_detail` (
  `id` int(11) NOT NULL,
  `sell_log_id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `selling_price` float NOT NULL,
  `buying_price` float NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_category` varchar(160) NOT NULL,
  `discount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sell_log_detail`
--

INSERT INTO `sell_log_detail` (`id`, `sell_log_id`, `product_name`, `selling_price`, `buying_price`, `quantity`, `product_category`, `discount`) VALUES
(1, 1, 'NE-Fuldani-26', 1790, 1300, 1, '10', 0),
(2, 1, 'NE-Real Flower-36', 260, 190, 1, '10', 0),
(3, 2, 'BEST-3', 1250, 750, 1, '6', 0),
(4, 3, 'Gents pass', 600, 375, 1, '8', 0),
(5, 4, 'PL-06', 1950, 1300, 1, '6', 0),
(6, 5, 'SS-909', 850, 500, 1, '5', 0),
(7, 6, 'SS-Plastic', 560, 350, 1, '5', 0),
(8, 7, 'JP-MB C', 680, 420, 1, '8', 0),
(9, 8, 'MC-25', 250, 120, 1, '3', 0),
(10, 8, 'MC-25', 250, 120, 1, '3', 0),
(11, 9, 'JT-121', 850, 500, 1, '3', 0),
(12, 10, 'MC-31', 110, 55, 1, '1', 0),
(13, 11, 'MC-31', 110, 55, 1, '1', 0),
(14, 12, 'returned product', -110, -55, 1, '3', 0),
(15, 13, 'returned product', -110, -55, 1, '3', 0),
(16, 14, 'BEST-1', 950, 500, 1, '6', 0),
(17, 15, 'BEST-3', 1250, 750, 1, '6', 0),
(18, 16, 'EAR TOP-2', 390, 195, 1, '4', 0),
(19, 16, 'AB-2', 1850, 1200, 1, '6', 0),
(20, 16, 'SGC-34', 180, 90, 1, '4', 0),
(21, 16, 'SGC-34', 180, 90, 1, '4', 0),
(22, 16, 'NE-Papush-11', 350, 180, 1, '10', 0),
(23, 17, 'TT-010', 350, 200, 1, '1', 0),
(24, 18, 'MS-99', 1390, 900, 1, '5', 0),
(25, 19, 'BEST-4', 1950, 1150, 1, '6', 0),
(26, 20, 'MC-26', 270, 150, 1, '3', 0),
(27, 20, 'SS-Plastic', 560, 350, 1, '5', 0),
(28, 21, 'UV-01', 420, 210, 1, '7', 0),
(29, 22, 'SS-808-16', 1280, 800, 1, '5', 0),
(30, 23, 'BEST-1', 950, 500, 1, '6', 0),
(31, 24, 'TT-012', 370, 210, 1, '1', 0),
(32, 25, 'MC-31', 110, 55, 1, '1', 0),
(33, 26, 'MC-31', 110, 55, 1, '1', 0),
(34, 27, 'SS-299-4', 1050, 700, 1, '5', 0),
(35, 28, 'WCS-B801', 1150, 690, 1, '5', 0),
(36, 29, 'Netro-2', 260, 140, 1, '3', 0),
(37, 30, 'RA-1', 250, 223, 1, '6', 0),
(38, 31, 'SS-730', 1590, 950, 1, '5', 0),
(39, 32, 'DS-26', 1480, 900, 1, '5', 0),
(40, 33, 'PLS-1', 1450, 950, 1, '6', 0),
(41, 34, 'RA-04', 250, 175, 1, '6', 0),
(42, 35, 'LB-XZ-2', 880, 520, 1, '5', 0),
(43, 36, 'MOD-N862', 1240, 700, 1, '5', 0),
(44, 37, 'MC-27', 120, 55, 1, '12', 0),
(45, 38, 'SS-Plastic', 560, 350, 1, '5', 0),
(46, 39, 'JT-9903', 590, 320, 1, '5', 0),
(47, 40, 'SS-Plastic', 560, 350, 1, '5', 0),
(48, 41, 'SS-A302', 850, 500, 1, '5', 0),
(49, 42, 'UV-03', 110, 65, 1, '7', 0),
(50, 43, 'PLS-08', 1350, 900, 1, '6', 0),
(51, 44, 'MC-31', 110, 55, 1, '1', 0),
(52, 45, 'MC-31', 110, 55, 1, '1', 0),
(53, 46, 'SS-730', 1590, 950, 1, '5', 0),
(54, 47, 'AB-01', 1820, 1150, 1, '6', 0),
(55, 48, 'NS-01', 650, 300, 1, '5', 0),
(56, 49, 'MC-31', 110, 55, 1, '1', 0),
(57, 50, 'SS-B206', 850, 500, 1, '5', 0),
(58, 51, 'TT-01', 370, 210, 1, '1', 0),
(59, 52, 'SS-182', 720, 420, 1, '5', 0),
(60, 52, 'NE-Flower Tob -30', 450, 300, 1, '10', 0),
(61, 53, 'SS-Plastic', 560, 350, 1, '5', 0),
(62, 54, 'PLS-4', 1890, 1200, 1, '6', 0),
(63, 55, 'ZT-S10', 380, 235, 1, '2', 0),
(64, 56, 'SS-1389', 1090, 750, 1, '5', 0),
(65, 57, 'TT-1', 340, 200, 1, '1', 0),
(66, 58, 'MC-31', 110, 55, 1, '1', 0),
(67, 59, 'BEST-2', 2150, 1300, 1, '6', 0),
(68, 60, 'SS-Plastic', 560, 350, 1, '5', 0),
(69, 61, 'TT-05', 350, 200, 1, '1', 0),
(70, 62, 'IF-04', 890, 500, 1, '1', 0);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `supplier_id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `supply`
--

CREATE TABLE `supply` (
  `supply_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `payment_status` varchar(160) NOT NULL,
  `supply_status` varchar(160) NOT NULL,
  `supply_order_time` datetime NOT NULL,
  `deliver_time` datetime NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `cost_cost_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `user_status` int(3) NOT NULL,
  `last_login` datetime NOT NULL,
  `user_detail_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `password`, `user_status`, `last_login`, `user_detail_id`) VALUES
(1, 'asif', '5f4dcc3b5aa765d61d8327deb882cf99', 1, '2018-09-01 00:52:03', 1),
(2, 'tuhin', 'e10adc3949ba59abbe56e057f20f883e', 0, '2018-07-19 22:58:29', 2),
(3, 'admin', '4297f44b13955235245b2497399d7a93', 1, '2018-09-16 10:55:31', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user-detail_user_group`
--

CREATE TABLE `user-detail_user_group` (
  `user_detail_id` int(11) NOT NULL,
  `user_group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `user_detail_id` int(11) NOT NULL,
  `verification_code` varchar(60) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `email` varchar(80) NOT NULL,
  `registered` datetime NOT NULL,
  `phone` varchar(45) NOT NULL,
  `address` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`user_detail_id`, `verification_code`, `first_name`, `last_name`, `email`, `registered`, `phone`, `address`) VALUES
(1, 'None', 'Asif', 'Hassan', 'asif@mail.com', '2018-07-18 00:29:20', '0165586545', 'Dhaka, Bangladesh'),
(2, 'None', 'Tuhin', 'Khan', 't@g.c', '2018-07-18 00:56:08', '0173707050', 'France'),
(3, 'None', 'admin', 'nobabee', 'admin@gmail.com', '2018-09-02 13:54:24', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
  `user_group_id` int(11) NOT NULL,
  `user_group` varchar(80) NOT NULL,
  `group_description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`user_group_id`, `user_group`, `group_description`) VALUES
(1, 'admin', 'All privileges are allowed');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_permission`
--

CREATE TABLE `user_group_permission` (
  `user_group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch_product`
--
ALTER TABLE `branch_product`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `branch_store_id` (`branch_store_id`);

--
-- Indexes for table `branch_store`
--
ALTER TABLE `branch_store`
  ADD PRIMARY KEY (`branch_store_id`),
  ADD KEY `branch_manager_user_id` (`branch_manager_user_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `company_info`
--
ALTER TABLE `company_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`cost_id`);

--
-- Indexes for table `coustomer`
--
ALTER TABLE `coustomer`
  ADD PRIMARY KEY (`coustomer_id`);

--
-- Indexes for table `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `discount_product`
--
ALTER TABLE `discount_product`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `discount_id` (`discount_id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `supply_id` (`supply_id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `coustomer_id` (`coustomer_id`),
  ADD KEY `user_user_id` (`user_used_id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `permission`
--
ALTER TABLE `permission`
  ADD PRIMARY KEY (`permission_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `product_variant`
--
ALTER TABLE `product_variant`
  ADD PRIMARY KEY (`product_variant_id`),
  ADD KEY `fk_product_id` (`product_id`) USING BTREE;

--
-- Indexes for table `returned_order`
--
ALTER TABLE `returned_order`
  ADD PRIMARY KEY (`returned_order_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `returned_product`
--
ALTER TABLE `returned_product`
  ADD KEY `product_id` (`product_id`),
  ADD KEY `returned_order_id` (`returned_order_id`);

--
-- Indexes for table `sell_log`
--
ALTER TABLE `sell_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sell_log_detail`
--
ALTER TABLE `sell_log_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sell_log_id` (`sell_log_id`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `supply`
--
ALTER TABLE `supply`
  ADD PRIMARY KEY (`supply_id`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `cost_cost_id` (`cost_cost_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `user_detail_id` (`user_detail_id`);

--
-- Indexes for table `user-detail_user_group`
--
ALTER TABLE `user-detail_user_group`
  ADD KEY `user_detail_id` (`user_detail_id`),
  ADD KEY `user_group_id` (`user_group_id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`user_detail_id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
  ADD PRIMARY KEY (`user_group_id`);

--
-- Indexes for table `user_group_permission`
--
ALTER TABLE `user_group_permission`
  ADD KEY `user_group_id` (`user_group_id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch_store`
--
ALTER TABLE `branch_store`
  MODIFY `branch_store_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `company_info`
--
ALTER TABLE `company_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cost`
--
ALTER TABLE `cost`
  MODIFY `cost_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coustomer`
--
ALTER TABLE `coustomer`
  MODIFY `coustomer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `discount`
--
ALTER TABLE `discount`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `permission`
--
ALTER TABLE `permission`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=589;

--
-- AUTO_INCREMENT for table `product_variant`
--
ALTER TABLE `product_variant`
  MODIFY `product_variant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=587;

--
-- AUTO_INCREMENT for table `returned_order`
--
ALTER TABLE `returned_order`
  MODIFY `returned_order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `returned_product`
--
ALTER TABLE `returned_product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `sell_log`
--
ALTER TABLE `sell_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `sell_log_detail`
--
ALTER TABLE `sell_log_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `supplier`
--
ALTER TABLE `supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `supply`
--
ALTER TABLE `supply`
  MODIFY `supply_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `user_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
  MODIFY `user_group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `branch_product`
--
ALTER TABLE `branch_product`
  ADD CONSTRAINT `fk_branch_store_id_bs` FOREIGN KEY (`branch_store_id`) REFERENCES `branch_store` (`branch_store_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_id_produ` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `branch_store`
--
ALTER TABLE `branch_store`
  ADD CONSTRAINT `fk_user_id_ur` FOREIGN KEY (`branch_manager_user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `discount_product`
--
ALTER TABLE `discount_product`
  ADD CONSTRAINT `fk_discount_id_d` FOREIGN KEY (`discount_id`) REFERENCES `discount` (`discount_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_id_po` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `inventory`
--
ALTER TABLE `inventory`
  ADD CONSTRAINT `fk_product_id_prod` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_supply_id_s` FOREIGN KEY (`supply_id`) REFERENCES `supply` (`supply_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_coustomer_id_c` FOREIGN KEY (`coustomer_id`) REFERENCES `coustomer` (`coustomer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_user_id_u` FOREIGN KEY (`user_used_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD CONSTRAINT `fk_order_id_o` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_id_p` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `category_id_cat` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_variant`
--
ALTER TABLE `product_variant`
  ADD CONSTRAINT `product_var_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `returned_order`
--
ALTER TABLE `returned_order`
  ADD CONSTRAINT `fk_order_id_or` FOREIGN KEY (`order_id`) REFERENCES `order` (`order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `returned_product`
--
ALTER TABLE `returned_product`
  ADD CONSTRAINT `fk_product_id_pro` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_returned_order_id` FOREIGN KEY (`returned_order_id`) REFERENCES `returned_order` (`returned_order_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sell_log_detail`
--
ALTER TABLE `sell_log_detail`
  ADD CONSTRAINT `id_sl` FOREIGN KEY (`sell_log_id`) REFERENCES `sell_log` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `supply`
--
ALTER TABLE `supply`
  ADD CONSTRAINT `fk_cost_cost_id_c` FOREIGN KEY (`cost_cost_id`) REFERENCES `cost` (`cost_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_supplier_id_su` FOREIGN KEY (`supplier_id`) REFERENCES `supplier` (`supplier_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_detail_id` FOREIGN KEY (`user_detail_id`) REFERENCES `user_detail` (`user_detail_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user-detail_user_group`
--
ALTER TABLE `user-detail_user_group`
  ADD CONSTRAINT `fk_user_detail_id_dtgrp` FOREIGN KEY (`user_detail_id`) REFERENCES `user_detail` (`user_detail_id`),
  ADD CONSTRAINT `fk_user_group_id` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`user_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_group_permission`
--
ALTER TABLE `user_group_permission`
  ADD CONSTRAINT `fk_permission_id_p` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`permission_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_group_id_ug` FOREIGN KEY (`user_group_id`) REFERENCES `user_group` (`user_group_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
