<?php 
require_once('includes/header.php');
date_default_timezone_set('Asia/Dhaka');
$sell_log = array();
$given_date = date('d/m/Y');
$total_overall_discount = 0;
$total_selling_price=0;
$db = new Database();
$db->connect();
$sql = "SELECT sl.date, sld.product_name, (SELECT c.category_name FROM category as c WHERE c.category_id=sld.product_category) as category_name, (SELECT o.order_id FROM pos.order as o WHERE o.created=sl.date) as memo_id, ((sld.selling_price*sld.quantity)-sld.discount) as actual_sell_price, sld.quantity FROM sell_log_detail as sld LEFT JOIN sell_log as sl ON sl.id=sld.sell_log_id";
if( isset($_POST['datepicker']) ) {
    $given_date = $_POST['datepicker'];
    $from = date('Y-m-d 00:00:00', strtotime($given_date));
    // echo $from . '<br>';
    $to = date('Y-m-d 23:59:59', strtotime($given_date));
	// echo $to . '<br>';
	$given_date = date('d/m/Y', strtotime($given_date));
}else{
    $from = date('Y-m-d 00:00:00');
    // echo  $from . '<br>';
    $to = date('Y-m-d 23:59:59');
    // echo $to . '<br>';
}
$sql .= " WHERE sl.date BETWEEN '{$from}' AND '{$to}'";
$db->set_sql($sql);
if($db->get_all_rows()) {
	$sell_log = $db->get_result();
}

$sql = "SELECT sum(discount) as total_discount FROM sell_log WHERE date BETWEEN '{$from}' AND '{$to}'";
$db->set_sql($sql);
if($db->get_all_rows()) {
	$total_overall_discount = $db->get_result()[0]['total_discount'];
}
// echo 'from: ' . $from . '<br>to: ' . $to . '<br>';
// echo '<pre>'; print_r($total_overall_discount); echo '</pre>';
// echo '<pre>';print_r($sell_log);echo '</pre>';
?>

<link rel="stylesheet" href="css/vendor/jquery/jquery-ui.css">

<!-- LINE BELLOW THIS IS IMPORTANT FOR EXCEL EXPORT -->
<iframe id="txtArea1" style="display:none"></iframe>

<div class="container">
    <div class="row">
        <div class='col-12'>
            <form method="POST" action="#" autocomplete="off">
			  <div class="row">
			    <div class="col-row mr-2">
			      <!-- <label>DATE TO SHOW LOG:</label> -->
			      <input type="text" class="form-control" id="datepicker" placeholder="Select a day" name="datepicker">
			    </div>
			    <div class="col-row">
			    	<input class="btn btn-primary mr-2" type="submit" id="daily_report_date_picker" name="date_picker_submit" value="Update">
			    </div>
			    <div class="col-row">
			    	<button class="btn btn-outline-info" id="btnExport" name="export" onclick="fnExcelReport('daily_sale_report');">Export</button>
			    </div>
			  </div>
			</form>
        </div>
    </div>
</div>




<div class="container mt-4">
	<div class="row">
		<div class="col-12">
			<h2>Daily Sale Report</h2>
			<form>
			</form>
			<div class="table-responsive" id="daily_report">
				<table class="table table-striped table-bordered table-hover" id="daily_sale_report">
					<thead>
						<!-- <tr>
							<th colspan="6">
								<img src="images/logo.jpg" alt="brand-logo" style="width: 100%; height:100px;">
							</th>
						</tr> -->
						<tr>
							<th colspan="4">Nobabee Style daily sale report</th>
							<th colspan="2"><?php echo $given_date; ?></th>
						</tr>
						<tr>
							<th>SL#</th>
							<th>Category</th>
							<th>Product</th>
							<th>Quantity</th>
							<th>Memo#</th>
							<th>Sale Price</th>
						</tr>
					</thead>
					<tbody>
					<?php $itr = 1; foreach($sell_log as $sl): ?>
					<?php 
						$sp = ceil($sl['actual_sell_price']*100)/100;

					?>
						<tr>
							<td><?php echo $itr++; ?></td>
							<td><?php echo $sl['category_name']; ?></td>
							<td><?php echo $sl['product_name']; ?></td>	
							<td><?php echo $sl['quantity']; ?></td>
							<td><?php echo $sl['memo_id']; ?></td>
							<td><?php echo $sp; ?></td>
						</tr>
					<?php
						$total_selling_price+=$sp;
						endforeach;
					?>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><?php echo "<b>Sub-total</b>: ".ceil($total_selling_price*100)/100; ?></td>
						<td><?php echo '<b>Discount</b>: ' . ceil($total_overall_discount);?></td>
						<td><?php echo "<b>Total</b>: ".ceil(($total_selling_price-$total_overall_discount)*100)/100; ?></td>					
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>

<script src="js/vendor/jquery/jquery-ui.js"></script>
<script>
    $('#datepicker').datepicker({
        changeMonth: true,
        changeYear: true
    });
</script>

<?php require_once('includes/footer.php'); ?>