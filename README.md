# Point of sale
This system is developed for [Improve Group](http://www.improve-group.com/) Nobabee style
## Feature List
1. Sale product by authorized user
2. CRUD users
3. Generated Barcode for products
4. CRUD products
5. Custom report generate
6. Return product
7. Manage versatile dicount
Detail feature list [here](https://bitbucket.org/aoeasif/nobabee/src/master/doc/POS%20System%20Requrements.pdf)

## Tools
1. PHP
2. HTML, CSS, jQuery, JSON, JavaScript 
3. MySQL Database

